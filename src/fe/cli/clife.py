#!/usr/bin/python
# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.
# $Header: /var/cvsroot/gentoo/src/installer/src/fe/cli/clife.py,v 1.7 2005/10/15 17:24:46 agaffney Exp $

import sys
sys.path.append("../..")

import time
from optparse import OptionParser

import GLIException
import GLIClientController
import GLIInstallProfile
from GLILogger import Logger
import GLIUtility

clilog = None

def msg(s):
	print s
	if clilog != None:
		clilog.log(s)
	
def try_steps(steps,cc):
	try:
		do_steps(steps, cc)
		sys.stderr.write('\07')  #BEEP
	except GLIException.GLIException, e:
		sys.stderr.write('\07')  #BEEP
		error = e.get_error_msg()
		msg("Error during install: %s" % error)
		sys.exit(-1)

def main():
	clilog = Logger("/var/log/installer-cli.log")
	usage = "usage: %prog [options] INSTALL.xml"
	parser = OptionParser(usage)
	parser.add_option("-p", "--pretend", dest="pretend_install",action="store_true",help="Pretend",default=False)
	parser.add_option("-d", "--detailed", dest="detailed",action="store_true",help="Prints the subprogress info",default=False)
	global options
	(options, args) = parser.parse_args()
	
	if len(args) != 1:
		sys.stderr.write("You must specify the install profile!\n")
		sys.exit(-1)
	
	install_profile_xml_file = args[0]
	
	msg("Loading install profile: "+install_profile_xml_file)
	if not GLIUtility.get_uri( install_profile_xml_file, "/var/tmp/installprofile.xml" ):
		msg("Couldn't open install profile: "+install_profile_xml_file)
		sys.exit(-1)

	install_profile = GLIInstallProfile.InstallProfile()
	install_profile.parse("/var/tmp/installprofile.xml")	
	
	cc = GLIClientController.GLIClientController(install_profile, pretend=options.pretend_install, verbose=True)
	
	cc.set_install_profile(install_profile)
#	cc.start_install()
	if options.pretend_install:
		msg("Pretending ")

	msg("Serializing XML for test reasons.")
	f = open("/var/tmp/installprofile-serialized.xml","w")
	f.write(install_profile.serialize())
	f.close()

	steps = ['do_recommended_partitioning',
			'mount_local_partitions',
			'mount_network_shares',
			'unpack_stage_tarball',
			'update_config_files',
			'configure_make_conf',
			'prepare_chroot',
			'install_portage_tree',
			'stage1',
			'stage2',
			'set_root_password',
			'set_timezone',
			'emerge_kernel_sources',
			'build_kernel',
			'install_distcc',
			'install_mta',
			'install_logging_daemon',
			'install_cron_daemon',
			'install_filesystem_tools',
			'setup_network_post',
			'install_bootloader',
			'setup_and_run_bootloader',
			'update_config_files',
			'set_users',
			'install_packages',
			# services for startup need to come after installing extra packages
			# otherwise some of the scripts will not exist.
			'set_services',
			'run_post_install_script',
			'finishing_cleanup']

	try_steps(steps, cc)
	msg("Install completed!")
	sys.exit(0)

def do_steps(steps, cc):
	msg("Installation Started!")
	time.sleep(2)
	num_steps_completed = 1
	install_steps = list(steps)
	num_steps = len(install_steps)
	
	#Start off with one step.
	step_name = install_steps.pop(0)
	next_step = cc.get_step_info(step_name)
	s = "On step %d of %d. Current step: %s" % (num_steps_completed, num_steps, next_step)
	msg(s)
	cc.run_step(step_name)
	
	while 1:
		notification = cc.getNotification()
		if notification == None:
			time.sleep(1)
			continue
		notify_type = notification.get_type()
		notify_data = notification.get_data()
		if notify_type == "exception":
			msg("Exception ("+repr(notify_data)+") received:\n"+str(notify_data))
			raise GLIException.GLIException("DoStepFailure",'fatal','do_steps',str(notify_data))
			#This should be a RAISE so it gets caught above.
		elif notify_type == "progress":
			#SECONDARY UPDATIN' GOIN ON IN HERE
			if options.detailed:  #only show if in detailed mode.
				diff = (notify_data[0]*100)/num_steps
				msg("On step %d of %d. Current step: %s\n%s" % (num_steps_completed, num_steps, next_step, notify_data[1]))
		elif notify_type == "int":
			if notify_data == GLIClientController.NEXT_STEP_READY:
				if len(install_steps):
					step_name = install_steps.pop(0)
				else:
					return
				#print "Step name: " + step_name
				next_step = cc.get_step_info(step_name)
				i = (num_steps_completed*100)/num_steps
				s = "On step %d of %d. Current step: %s" % (num_steps_completed, num_steps, next_step)
				msg(s)
				num_steps_completed += 1
				cc.run_step(step_name)
				continue
	

if __name__ == "__main__":
	main()
