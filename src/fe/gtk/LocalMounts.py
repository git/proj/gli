# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk, gobject
from GLIScreen import *
import Widgets
import commands, string
import copy
from ProgressDialog import *
import Partitioning
import GLIUtility

class Panel(GLIScreen):

	title = _("Local Mounts")
	columns = []
	localmounts = []
	active_entry = -1
	_helptext = _("""
<b><u>Local Mounts</u></b>

Here, you can add partitions and special devices to mount during (and after) \
the install. All mountpoints are relative to /mnt/gentoo during the install.

To start, click the button labeled 'Add'. Enter a device name or select if from \
the list. If you entered a device name yourself, you'll need to fill in the \
type in the next field. Then enter the mountpoint and mount options (defaults to \
"defaults"). Click 'Update' to save the new mount.

To edit a mount that you've already added to the list list, select it by \
clicking on it. Edit any of the fields below and click the 'Update' button. You \
can remove it from the list by clicking the 'Delete' button.
""")

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		self.treedata = gtk.ListStore(gobject.TYPE_INT, gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING)
		self.treedatasort = gtk.TreeModelSort(self.treedata)
		self.treeview = gtk.TreeView(self.treedatasort)
		self.treeview.connect("cursor-changed", self.selection_changed)
		self.columns.append(gtk.TreeViewColumn(_("Device"), gtk.CellRendererText(), text=1))
		self.columns.append(gtk.TreeViewColumn(_("Type"), gtk.CellRendererText(), text=2))
		self.columns.append(gtk.TreeViewColumn(_("Mount Point"), gtk.CellRendererText(), text=3))
		self.columns.append(gtk.TreeViewColumn(_("Mount Options"), gtk.CellRendererText(), text=4))
		col_num = 0
		for column in self.columns:
			column.set_resizable(True)
			column.set_sort_column_id(col_num)
			self.treeview.append_column(column)
			col_num += 1
		self.treewindow = gtk.ScrolledWindow()
		self.treewindow.set_size_request(-1, 130)
		self.treewindow.set_shadow_type(gtk.SHADOW_IN)
		self.treewindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.treewindow.add(self.treeview)
		vert.pack_start(self.treewindow, expand=False, fill=False, padding=0)

		self.mount_info_box = gtk.HBox(False, 0)
		mount_info_table = gtk.Table(5, 3, False)
		mount_info_table.set_col_spacings(10)
		mount_info_table.set_row_spacings(6)
		mount_info_device_label = gtk.Label(_("Device:"))
		mount_info_device_label.set_alignment(0.0, 0.5)
		mount_info_table.attach(mount_info_device_label, 0, 1, 0, 1)
		self.mount_info_device = gtk.ComboBoxEntry(gtk.ListStore(gobject.TYPE_STRING))
		self.mount_info_device.connect("changed", self.device_changed)
		if not self.mount_info_device.get_text_column() == 0:
			self.mount_info_device.set_text_column(0)
		mount_info_table.attach(self.mount_info_device, 1, 2, 0, 1)
		mount_info_type_label = gtk.Label(_("Type:"))
		mount_info_type_label.set_alignment(0.0, 0.5)
		mount_info_table.attach(mount_info_type_label, 0, 1, 1, 2)
		self.mount_info_type = gtk.Entry()
		self.mount_info_type.set_width_chars(20)
		mount_info_table.attach(self.mount_info_type, 1, 2, 1, 2)
		mount_info_mountpoint_label = gtk.Label(_("Mount point:"))
		mount_info_mountpoint_label.set_alignment(0.0, 0.5)
		mount_info_table.attach(mount_info_mountpoint_label, 0, 1, 2, 3)
		self.mount_info_mountpoint = gtk.Entry()
		self.mount_info_mountpoint.set_width_chars(30)
		mount_info_table.attach(self.mount_info_mountpoint, 1, 2, 2, 3)
		mount_info_mountopts_label = gtk.Label(_("Mount options:"))
		mount_info_mountopts_label.set_alignment(0.0, 0.5)
		mount_info_table.attach(mount_info_mountopts_label, 0, 1, 3, 4)
		self.mount_info_mountopts = gtk.Entry()
		self.mount_info_mountopts.set_width_chars(30)
		mount_info_table.attach(self.mount_info_mountopts, 1, 2, 3, 4)
		self.mount_info_box.pack_start(mount_info_table, expand=False, fill=False)
		vert.pack_start(self.mount_info_box, expand=False, fill=False, padding=10)

		mount_button_box = gtk.HBox(False, 0)
		mount_button_add = gtk.Button(_(" _Add "))
		mount_button_add.connect("clicked", self.new_mount)
		mount_button_box.pack_start(mount_button_add, expand=False, fill=False, padding=10)
		self.mount_button_update = gtk.Button(_(" _Update "))
		self.mount_button_update.connect("clicked", self.update_mount)
		self.mount_button_update.set_sensitive(False)
		mount_button_box.pack_start(self.mount_button_update, expand=False, fill=False, padding=10)
		self.mount_button_delete = gtk.Button(_(" _Delete "))
		self.mount_button_delete.connect("clicked", self.delete_mount)
		self.mount_button_delete.set_sensitive(False)
		mount_button_box.pack_start(self.mount_button_delete, expand=False, fill=False, padding=10)
		vert.pack_start(mount_button_box, expand=False, fill=False, padding=10)

		self.partitions = {}
		for device in Partitioning.detect_devices():
			try:
				drive = Partitioning.Device(device, self.controller.cc.get_arch(), self.controller.install_profile)
				for part in drive:
					self.partitions[part['devnode']] = part['type']
			except:
				pass
		self.part_list = self.partitions.keys()
		self.part_list.sort()
		for part in self.part_list:
			self.mount_info_device.get_model().append([part])

		self.add_content(vert)

	def disable_all_fields(self):
		self.mount_info_device.get_child().set_text("")
		self.mount_info_type.set_text("")
		self.mount_info_mountpoint.set_text("")
		self.mount_info_mountopts.set_text("")
		self.mount_info_device.set_sensitive(False)
		self.mount_info_type.set_sensitive(False)
		self.mount_info_mountpoint.set_sensitive(False)
		self.mount_info_mountopts.set_sensitive(False)

	def enable_all_fields(self):
		self.mount_info_device.get_child().set_text("")
		self.mount_info_mountpoint.set_text("")
		self.mount_info_mountopts.set_text("")
		self.mount_info_device.set_sensitive(True)
		self.mount_info_type.set_sensitive(True)
		self.mount_info_mountpoint.set_sensitive(True)
		self.mount_info_mountopts.set_sensitive(True)

	def refresh_list_at_top(self):
		self.treedata.clear()
		for i, mount in enumerate(self.localmounts):
			self.treedata.append([i, mount['devnode'], mount['type'], mount['mountpoint'], mount['mountopts']])
		self.treedatasort = gtk.TreeModelSort(self.treedata)
		self.treeview.set_model(self.treedatasort)
		self.treeview.show_all()

	def selection_changed(self, treeview, data=None):
		treeselection = treeview.get_selection()
		treemodel, treeiter = treeselection.get_selected()
		row = treemodel.get(treeiter, 0)
		self.active_entry = row[0]
		mount = self.localmounts[self.active_entry]
		self.enable_all_fields()
		self.mount_info_device.get_child().set_text(mount['devnode'])
		self.mount_info_type.set_text(mount['type'])
		self.mount_info_mountpoint.set_text(mount['mountpoint'])
		self.mount_info_mountopts.set_text(mount['mountopts'])
		self.mount_button_update.set_sensitive(True)
		self.mount_button_delete.set_sensitive(True)

	def new_mount(self, button, data=None):
		self.active_entry = -1
		self.mount_button_update.set_sensitive(True)
		self.mount_button_delete.set_sensitive(False)
		self.enable_all_fields()
		self.mount_info_device.grab_focus()

	def update_mount(self, button):
		if self.mount_info_device.get_child().get_text().strip() == "":
			msgdialog = Widgets.Widgets().error_Box(_("Invalid Entry"), _("You must enter a value for the device field"))
			result = msgdialog.run()
			if result == gtk.RESPONSE_ACCEPT:
				msgdialog.destroy()
			return
		if not self.mount_info_type.get_text().strip() in ("swap", "linux-swap") and self.mount_info_mountpoint.get_text().strip() == "":
			msgdialog = Widgets.Widgets().error_Box(_("Invalid Entry"), _("You must enter a mountpoint"))
			result = msgdialog.run()
			if result == gtk.RESPONSE_ACCEPT:
				msgdialog.destroy()
			return
		if self.mount_info_type.get_text().strip() == "":
			self.mount_info_type.set_text("auto")
		if self.mount_info_mountopts.get_text().strip() == "":
			self.mount_info_mountopts.set_text("defaults")
		if self.active_entry == -1:
			self.localmounts.append({ 'devnode': self.mount_info_device.get_child().get_text(), 'type': self.mount_info_type.get_text(), 'mountpoint': self.mount_info_mountpoint.get_text(), 'mountopts': self.mount_info_mountopts.get_text() })
			self.active_entry = -1
			self.mount_button_update.set_sensitive(False)
			self.mount_button_delete.set_sensitive(False)
		else:
			self.localmounts[self.active_entry]['devnode'] = self.mount_info_device.get_child().get_text()
			self.localmounts[self.active_entry]['type'] = self.mount_info_type.get_text()
			self.localmounts[self.active_entry]['mountpoint'] = self.mount_info_mountpoint.get_text()
			self.localmounts[self.active_entry]['mountopts'] = self.mount_info_mountopts.get_text()
		self.refresh_list_at_top()
		self.disable_all_fields()

	def delete_mount(self, button):
		self.localmounts.pop(self.active_entry)
		self.active_entry = -1
		self.mount_button_update.set_sensitive(False)
		self.mount_button_delete.set_sensitive(False)
		self.refresh_list_at_top()
		self.disable_all_fields()

	def device_changed(self, comboentry):
		self.mount_info_type.set_text(self.partitions.get(self.mount_info_device.get_child().get_text(), ""))

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = True
		self.controller.SHOW_BUTTON_FORWARD = True
		self.localmounts = copy.deepcopy(self.controller.install_profile.get_mounts())
		self.refresh_list_at_top()
		self.disable_all_fields()
		self.mount_button_update.set_sensitive(False)
		self.mount_button_delete.set_sensitive(False)

	def next(self):
		for mount in self.localmounts:
			if mount['mountpoint'] == "/":
				break
		else:
			msgdlg = gtk.MessageDialog(parent=self.controller.window, type=gtk.MESSAGE_WARNING, buttons=gtk.BUTTONS_OK, message_format=_("You must specify a mount at / to continue"))
			resp = msgdlg.run()
			msgdlg.destroy()
			return
		self.controller.install_profile.set_mounts(self.localmounts)
		# Set networkless defaults for stage
		self.controller.install_profile.set_install_stage(None, 3, None)
		self.controller.install_profile.set_dynamic_stage3(None, True, None)
		self.controller.install_profile.set_grp_install(None, True, None)
		# Set networkless defaults for portage tree
		self.controller.install_profile.set_portage_tree_snapshot_uri(None, GLIUtility.get_cd_snapshot_uri(), None)
		self.controller.install_profile.set_portage_tree_sync_type(None, "snapshot", None)

		progress = ProgressDialog(self.controller, ("mount_local_partitions", "unpack_stage_tarball", "prepare_chroot", "install_portage_tree", "configure_make_conf"), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("RootPass")
		else:
			GLIScreen.progress_callback(self, result, data)

	def previous(self):
		self.controller.install_profile.set_mounts(self.localmounts)
		self.controller.load_screen("Partition")
