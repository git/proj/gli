# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
import GLIUtility
from ProgressDialog import *

class Panel(GLIScreen):

	title = _("Root Password")
	_helptext = _("""
<b><u>Root Password</u></b>

Enter the password for the root user in your new install. Enter it again to \
confirm it (to prevent typos).
""")

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)

		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		hbox = gtk.HBox(False, 0)
		label = gtk.Label()
		label.set_markup(_('<b>Enter a root password</b>'))
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		vert.pack_start(hbox, expand=False, fill=False, padding=10)

		table = gtk.Table(2, 2, False)
		table.set_row_spacings(5)
		table.set_col_spacings(10)

		label = gtk.Label(_("Password"))
		label.set_alignment(0.0, 0.5)
		table.attach(label, 0, 1, 0, 1)
		self.entry_password = gtk.Entry()
		self.entry_password.set_width_chars(30)
		self.entry_password.set_visibility(False)
		table.attach(self.entry_password, 1, 2, 0, 1)

		label = gtk.Label(_("Confirm"))
		label.set_alignment(0.0, 0.5)
		table.attach(label, 0, 1, 1, 2)
		self.entry_confirm = gtk.Entry()
		self.entry_confirm.set_width_chars(30)
		self.entry_confirm.set_visibility(False)
		table.attach(self.entry_confirm, 1, 2, 1, 2)

		hbox = gtk.HBox(False)
		hbox.set_border_width(15)
		hbox.pack_start(table, fill=False, expand=False, padding=0)
		vert.pack_start(hbox, expand=False, fill=False, padding=20)

		self.add_content(vert)

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = True

	def next(self):
		if self.entry_password.get_text() == self.entry_confirm.get_text():
			if self.entry_password.get_text()[:3] == "$1$":
				root_hash = self.entry_password.get_text()
			else:
				root_hash = GLIUtility.hash_password(self.entry_password.get_text())
			self.controller.install_profile.set_root_pass_hash(None, root_hash, None)
		else:
			msgdlg = gtk.MessageDialog(parent=self.controller.window, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format=_("The passwords you entered do not match!"))
			msgdlg.run()
			msgdlg.destroy()
			return
		progress = ProgressDialog(self.controller, (_("set_root_password"), ), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("Timezone")
		else:
			GLIScreen.progress_callback(self, result, data)
