# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
from ProgressDialog import *

class Panel(GLIScreen):

	title = "Kernel Sources"
	active_selection = None
	radio_sources = {}
	_helptext = """
<b><u>Kernel Sources</u></b>

You have a few different choices when it comes to kernel source packages.

The most popular (and usually most stable) one is <i>gentoo-sources</i>. This \
is the vanilla kernel source tree with the Gentoo kernel patchset applied. \
These patches are usually non-invasive and add desired functionality.

The other standard choice is <i>vanilla-sources</i>, which is the unmolested \
vanilla kernel source tree as found at http://kernel.org/.

If you are a bit more paranoid about security, you use <i>hardened-sources</i>, \
which is similar to gentoo-sources, but it has additional functionality patched \
in like PaX and Grsecurity. You should probably read up at \
http://hardened.gentoo.org/ before you use these.

These choices are only available in Advanced mode.
"""
	sources = {
	  'all':   [ "gentoo-sources", "vanilla-sources", "hardened-sources" ],
	  'ppc':   [ "ppc-sources" ],
	  'hppa':  [ "hppa-sources" ],
	  'mips':  [ "mips-sources" ],
	  'sparc': [ "sparc-sources" ]
	}

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		if self.controller.install_type == "standard":
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Your kernel will be built using gentoo-sources</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)
		else:
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Choose which kernel sources you want to use to build your kernel</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			tmpsources = self.sources['all'] + self.sources.get(self.controller.cc.get_arch(), [])
			for source in tmpsources:
				hbox = gtk.HBox(False, 0)
				if source == tmpsources[0]:
					self.radio_sources[source] = gtk.RadioButton(None, source)
				else:
					self.radio_sources[source] = gtk.RadioButton(self.radio_sources[tmpsources[0]], source)
				self.radio_sources[source].set_name(source)
				self.radio_sources[source].connect("toggled", self.sources_selected, source)
				hbox.pack_start(self.radio_sources[source], expand=False, fill=False, padding=20)
				vert.pack_start(hbox, expand=False, fill=False, padding=20)

		self.add_content(vert)

	def sources_selected(self, widget, data=None):
		self.active_selection = data

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = True
		self.controller.SHOW_BUTTON_FORWARD = True
		if self.controller.install_type != "standard":
			self.active_selection = self.controller.install_profile.get_kernel_source_pkg() or "gentoo-sources"
			self.radio_sources[self.active_selection].set_active(True)

	def next(self):
		if self.controller.install_type == "standard":
			self.controller.install_profile.set_kernel_source_pkg(None, "gentoo-sources", None)
		else:
			self.controller.install_profile.set_kernel_source_pkg(None, self.active_selection, None)
		progress = ProgressDialog(self.controller, ("emerge_kernel_sources", ), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("KernelConfig")
		else:
			GLIScreen.progress_callback(self, result, data)

	def previous(self):
		self.controller.load_screen("Kernel")
