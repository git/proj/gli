#!/usr/bin/python

# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import sys
sys.path.append("../..")

import os
import GLIInstallProfile
import GLIClientController
import GLIUtility
from SplashScreen import SplashScreen
import gtk
import gettext
import TextBufferMarkup
from ProgressDialog import *


try:
	gettext.install('gli-gtk', './languages')
	translator = gettext.translation('gli-gtk', './languages')
	_ = translator.gettext
except:
	_ = gettext.gettext

class Installer:

	SHOW_BUTTON_FORWARD = 1
	SHOW_BUTTON_BACK = 1
	install_profile_xml_file = ""
	install_window = None
#	install_type = "standard"
	install_type = "networkless"
	cur_screen = None
	install_done = False

	screens = {
#	  "InstallMode": None,
	  "Partition": None,
	  "LocalMounts": None,
#	  "NetworkMounts": None,
#	  "Stage": None,
#	  "PortageTree": None,
#	  "MakeDotConf": None,
	  "RootPass": None,
	  "Timezone": None,
#	  "Kernel": None,
#	  "KernelSources": None,
#	  "KernelConfig": None,
	  "Networking": None,
#	  "Logger": None,
#	  "CronDaemon": None,
#	  "Bootloader": None,
	  "Users": None,
	  "ExtraPackages": None,
	  "StartupServices": None,
	  "OtherSettings": None,
	  "InstallDone": None,
	  "InstallFailed": None
	}

	bootloaders = {
	  'none':  [ "none" ],
	  'x86':   [ "grub", "lilo" ],
	  'amd64': [ "grub" ],
	  'ppc':   [ "yaboot" ],
	  'hppa':  [ "palo" ],
	  'mips':  [ "arcload", "arcboot", "colo" ],
	  'sparc': [ "silo" ]
	}

	def __init__(self):
		self.window = None
		self.panel = None
		self._cur_panel = 0
		self.__full_path = self.get_current_path()

		# Show splash screen
		self.splash = SplashScreen(self.__full_path)
		self.splash.show()
		while gtk.events_pending():
			gtk.main_iteration()

		# Import screen modules
		for screen in self.screens:
			self.screens[screen] = __import__(screen)

		# Instantiate installer objects
		self.install_profile = GLIInstallProfile.InstallProfile()
		if '-d' in sys.argv or '--debug' in sys.argv:
			# Enable debug mode via commandline argument
			self.install_profile.set_verbose(None, True, None)
		self.cc = GLIClientController.GLIClientController(self.install_profile)

		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.title = _("Gentoo Linux Installer")
		self.window.realize()
		self.window.connect("delete_event", self.delete_event)
		self.window.connect("destroy", self.destroy)
		self.window.set_border_width(0)
		self.window.set_default_size(800,600)
		self.window.set_geometry_hints(None, min_width=800, min_height=600, max_width=800, max_height=600)
		self.window.set_title(_("Gentoo Linux Installer"))
		self.globalbox = gtk.VBox(False, 0)
		self.window.add(self.globalbox)

		# Banner image
		self.headerbox = gtk.HBox(False, 0)
		headerimg = gtk.Image()
		headerimg.set_from_file(self.__full_path + '/installer-banner-800x64.png')
		self.headerbox.pack_start(headerimg, padding=0)
		self.globalbox.pack_start(self.headerbox, expand=False, fill=False, padding=0)

		# Progress bar
#		progress_box = gtk.HBox(False, 0)
#		progress_box.pack_start(gtk.Label("Install Progress"), expand=False, fill=False, padding=10)
#		self.progress_bar = gtk.ProgressBar()
#		progress_box.pack_start(self.progress_bar, expand=True, fill=True, padding=10)
#		self.globalbox.pack_start(progress_box, expand=False, fill=False, padding=5)
#		self.globalbox.pack_start(gtk.HSeparator(), expand=False, fill=False, padding=0)

		# Top box
		self.topbox = gtk.HBox(False, 0)
		self.globalbox.pack_start(self.topbox, expand=True, fill=True, padding=5)

		# Left frame
		self.leftframe = gtk.Frame()
		self.leftframe.set_size_request(200, -1)
		self.leftframe.set_shadow_type(gtk.SHADOW_ETCHED_OUT)
		self.textbuff = TextBufferMarkup.PangoBuffer()
		self.textview = gtk.TextView(self.textbuff)
		self.textview.set_editable(False)
		self.textview.set_wrap_mode(gtk.WRAP_WORD)
		self.textview.set_left_margin(5)
		self.textview.set_right_margin(5)
		self.textview.set_sensitive(False)
		self.textviewscroll = gtk.ScrolledWindow()
		self.textviewscroll.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		self.textviewscroll.add(self.textview)
		self.leftframe.add(self.textviewscroll)
		self.topbox.pack_start(self.leftframe, expand=True, fill=True, padding=5)

		# Right frame
		self.rightframe = gtk.VBox(False, 0)
		self.rightframe.set_size_request(570, -1)
		self.topbox.pack_end(self.rightframe, expand=True, fill=True, padding=5)
		self.globalbox.show_all();

		# Bottom box
		self.bottombox = gtk.HBox(False, 0)
		self.globalbox.pack_end(self.bottombox, expand=False, fill=False, padding=5)
		self.globalbox.pack_end(gtk.HSeparator(), expand=False, fill=False, padding=0)

		buttons_info = [ ('exit', _(" _Exit "), '/button_images/stock_exit.png', self.exit_button, 'start'),
#		                 ('load', _(" _Load "), '/button_images/stock_open.png', self.load_button, 'start'),
#		                 ('save', _(" _Save "), '/button_images/stock_save.png', self.save_button, 'start'),
		                 ('log', _(" _View Log "), '', self.log_button, 'start'),
		                 ('next', _(" _Next "), '/button_images/stock_right.png', self.next, 'end'),
		                 ('previous', _(" _Previous "), '/button_images/stock_left.png', self.previous, 'end')
                               ]
		self.buttons = {}
		for button in buttons_info:
			self.buttons[button[0]] = gtk.Button()
			tmpbuttonbox = gtk.HBox(False, 0)
			if button[2]:
				tmpbuttonimg = gtk.Image()
				tmpbuttonimg.set_from_file(self.__full_path + button[2])
				tmpbuttonbox.pack_start(tmpbuttonimg)
			tmpbuttonlabel = gtk.Label(button[1])
			tmpbuttonlabel.set_use_underline(True)
			tmpbuttonbox.pack_start(tmpbuttonlabel)
			self.buttons[button[0]].add(tmpbuttonbox)
			self.buttons[button[0]].connect("clicked", button[3], None)
			if button[4] == "start":
				self.bottombox.pack_start(self.buttons[button[0]], expand=False, fill=False, padding=5)
			else:
				self.bottombox.pack_end(self.buttons[button[0]], expand=False, fill=False, padding=5)

		self.splash.destroy()
		self.window.show_all()
		self.load_screen("Partition")

	def redraw_buttons(self):
		self.bottombox.hide_all()
		self.buttons['next'].set_sensitive(self.SHOW_BUTTON_FORWARD)
		self.buttons['previous'].set_sensitive(self.SHOW_BUTTON_BACK)
		self.bottombox.show_all()

	def get_current_path(self):
		# this will return the absolute path to the
		# directory containing this file
		# it will only work if this file is imported somewhere,
		# not if it is run directly (__file__ will be undefined)
		import os.path
		return os.path.abspath(os.path.dirname(__file__))

	def load_screen(self, name):
		if self.cur_screen:
			self.rightframe.remove(self.cur_screen)
			del self.cur_screen
		self.cur_screen = self.screens[name].Panel(self)
#		percent = int((i) / float(len(self.screens) - 1) * 100)
#		self.progress_bar.set_fraction(float(percent) / 100)
#		self.progress_bar.set_text(str(percent) + "%")
		self.rightframe.add(self.cur_screen)
		self.cur_screen.activate()
		helptext = self.cur_screen._helptext
#		helptext = " ".join(helptext.split("\n"))
#		helptext = "\n".join(helptext.split('<br>'))
		self.textbuff.set_text(helptext)
		self.rightframe.show_all()
		self.redraw_buttons()

	def run(self):
		gtk.gdk.threads_init()
		gtk.main()

	def previous(self, widget, data=None):
		self.cur_screen.previous()

	def next(self, widget, data=None):
		self.cur_screen.next()

	def exit_button(self, widget, data=None):
		if not self.install_done:
			msgdlg = gtk.MessageDialog(parent=self.window, type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO, message_format=_("Are you sure you want to exit?"))
			resp = msgdlg.run()
			msgdlg.destroy()
			if resp == gtk.RESPONSE_YES:
				msgdlg = gtk.MessageDialog(parent=self.window, type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO, message_format=_("Do you want the installer to clean up after itself before exiting?"))
				resp = msgdlg.run()
				msgdlg.destroy()
				if resp == gtk.RESPONSE_YES:
					progress = ProgressDialog(self, ("install_failed_cleanup", ), self.exit_callback)
					progress.run()
				else:
					self.exit()
		else:
			self.exit()

	def exit_callback(self, result):
		self.exit()

	def load_button(self, widget, data=None):
		filesel = gtk.FileSelection(_("Select the install profile to load"))
		if self.install_profile_xml_file == "":
			filesel.set_filename("installprofile.xml")
		else:
			filesel.set_filename(self.install_profile_xml_file)
		resp = filesel.run()
		filename = filesel.get_filename()
		filesel.destroy()
		if resp == gtk.RESPONSE_OK:
			self.install_profile_xml_file = filename
			try:
				tmp_install_profile = GLIInstallProfile.InstallProfile()
				tmp_install_profile.parse(self.install_profile_xml_file)
				self.install_profile = tmp_install_profile
				msgdlg = gtk.MessageDialog(parent=self.window, type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_OK, message_format=_("Install profile loaded successfully!"))
				msgdlg.run()
				msgdlg.destroy()
			except:
				errdlg = gtk.MessageDialog(parent=self.window, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format=_("An error occured loading the install profile"))
				errdlg.run()
				errdlg.destroy()

	def save_button(self, widget, data=None):
		filesel = gtk.FileSelection(_("Select the location to save the install profile"))
		if self.install_profile_xml_file == "":
			filesel.set_filename("installprofile.xml")
		else:
			filesel.set_filename(self.install_profile_xml_file)
		resp = filesel.run()
		filename = filesel.get_filename()
		filesel.destroy()
		if resp == gtk.RESPONSE_OK:
			self.install_profile_xml_file = filename
			try:
				configuration = open(filename, "w")
				configuration.write(self.install_profile.serialize())
				configuration.close()
				msgdlg = gtk.MessageDialog(parent=self.window, type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_OK, message_format=_("Install profile saved successfully!"))
				msgdlg.run()
				msgdlg.destroy()
			except:
				errdlg = gtk.MessageDialog(parent=self.window, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format=_("An error occured saving the install profile"))
				errdlg.run()
				errdlg.destroy()

	def log_button(self, widget, data=None):
		if os.path.isfile("/var/log/install.log"):
			os.system("xterm -e 'tail -f /var/log/install.log' &")
		elif os.path.isfile("/var/log/install.log.failed"):
			os.system("xterm -e 'tail -f /var/log/install.log.failed' &")
		else:
			errdlg = gtk.MessageDialog(parent=self.window, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format=_("Could not find the log file"))
			errdlg.run()
			errdlg.destroy()

	def delete_event(self, widget, event, data=None):
		self.exit_button(self.buttons['exit'])
		return True

	def destroy(self, widget, data=None):
		gtk.main_quit()
		return True
	
	def exit(self, status=0):
		gtk.main_quit()
		sys.exit(status)

if __name__ == "__main__":
	install = Installer()
	install.run()
