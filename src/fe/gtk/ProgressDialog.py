# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk, pango, gobject
from gettext import gettext as _
from GLIClientController import NEXT_STEP_READY

PROGRESS_DONE = 1
PROGRESS_CANCEL = 2
PROGRESS_EXCEPTION = 3

class ProgressDialog(gtk.Window):

	def __init__(self, controller, install_steps, callback):
		gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)

		self.controller = controller
		self.install_steps = list(install_steps)
		self.callback = callback

		self.connect("delete_event", self.delete_event)
		self.connect("destroy", self.destroy_event)
		self.set_default_size(500, -1)
		self.set_resizable(True)
		self.set_title(_("Gentoo Linux Installer - Installation Progress"))
		self.set_modal(True)
		self.set_transient_for(controller.window)

		self.globalbox = gtk.VBox(False, 0)
		self.globalbox.set_border_width(10)
		hbox = gtk.HBox(False)
		step_label = gtk.Label(_("Step:"))
		hbox.pack_start(step_label, expand=False, fill=False, padding=0)
		self.step_text = gtk.Label()
		self.step_text.set_text(_("Step description here"))
		hbox.pack_start(self.step_text, expand=False, fill=False, padding=15)
		self.globalbox.pack_start(hbox, expand=True, fill=False, padding=0)
		self.progress_bar = gtk.ProgressBar()
		self.globalbox.pack_start(self.progress_bar, expand=True, fill=False, padding=5)
		hbox = gtk.HBox(True)
		self.cancel_button = gtk.Button(label=_("_Cancel"))
		self.cancel_button.connect("clicked", self.cancel_clicked)
		if not self.callback:
			self.cancel_button.set_sensitive(False)
		hbox.pack_start(self.cancel_button, expand=False, fill=False)
		self.globalbox.pack_end(hbox, expand=False, fill=False, padding=0)
		self.add(self.globalbox)

	def cancel_clicked(self, button):
		msgdlg = gtk.MessageDialog(parent=self, type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO, message_format=_("Do you really want to cancel before the install is complete?"))
		resp = msgdlg.run()
		msgdlg.destroy()
		if resp == gtk.RESPONSE_YES:
			self.destroy()
			if self.callback:
				self.callback(PROGRESS_CANCEL)

	def run(self):
		self.show_all()
		self.call_next_step()
		gobject.timeout_add(500, self.poll_notifications)

	def delete_event(self, widget, event):
		self.cancel_clicked(self.cancel_button)
		return True

	def destroy_event(self, widget, data=None):
		return True

	def call_next_step(self):
		step_name = self.install_steps.pop(0)
		step_descr = self.controller.cc.get_step_info(step_name)
		self.step_text.set_text(step_descr)
		self.progress_bar.set_fraction(0)
		self.progress_bar.set_text(_("Working..."))
		self.controller.cc.run_step(step_name)

	def poll_notifications(self):
		notification = self.controller.cc.getNotification()
		if not notification:
			return True
		ntype = notification.get_type()
		ndata = notification.get_data()
		if ntype == "exception":
			self.destroy()
			if self.callback:
				self.callback(PROGRESS_EXCEPTION, ndata)
			return False
		elif ntype == "int":
			if ndata == NEXT_STEP_READY:
				self.progress_bar.set_fraction(1)
				self.progress_bar.set_text(_("Done"))
				if not len(self.install_steps):
					self.destroy()
					if self.callback:
						self.callback(PROGRESS_DONE)
					return False
				self.call_next_step()
		elif ntype == "progress":
			self.progress_bar.set_fraction(ndata[0])
			if len(ndata[1]) > 80:
				ndata[1] = ndata[1][0:80] + "..."
			self.progress_bar.set_text(ndata[1])
			return True
		return True
