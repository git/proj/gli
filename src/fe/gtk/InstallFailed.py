# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *

class Panel(GLIScreen):

	_helptext = _("""
<b><u>Install Failed</u></b>

Your install has failed for one of numerous reasons. You can find the error in \
the logfile at /var/log/install.log.failed. Once you determine that the error was \
not caused by you, please file a bug at http://bugs.gentoo.org/ in the \
Gentoo Release Media product and the Installer component.
""")

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		self.controller.install_done = True

		hbox = gtk.HBox(False, 0)
		label = gtk.Label()
		label_markup = _('<b>Your install has failed.</b>')
		label.set_markup(label_markup)
		label.set_line_wrap(True)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		vert.pack_start(hbox, expand=False, fill=False, padding=10)

		self.add_content(vert)

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = False
