# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
from ProgressDialog import *

class Panel(GLIScreen):

	title = "Cron Daemon"
	active_selection = None
	radio_crons = {}
	_helptext = """
<b><u>Cron Daemon</u></b>

Pick a cron daemon. The most common choice is vixie-cron. This option is \
not available in Networkless mode.
"""
	crons = [ "vixie-cron", "fcron", "dcron", "None" ]

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		if self.controller.install_type == "networkless":
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Your cron daemon will be %s</b>' % self.crons[0])
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)
		else:
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Choose your cron daemon</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			for cron in self.crons:
				hbox = gtk.HBox(False, 0)
				if cron == self.crons[0]:
					self.radio_crons[cron] = gtk.RadioButton(None, cron)
				else:
					self.radio_crons[cron] = gtk.RadioButton(self.radio_crons[self.crons[0]], cron)
				self.radio_crons[cron].set_name(cron)
				self.radio_crons[cron].connect("toggled", self.cron_selected, cron)
				hbox.pack_start(self.radio_crons[cron], expand=False, fill=False, padding=20)
				vert.pack_start(hbox, expand=False, fill=False, padding=20)

		self.add_content(vert)
		self.boot_devices = None

	def cron_selected(self, widget, data=None):
		self.active_selection = data

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = True
		self.controller.SHOW_BUTTON_FORWARD = True
		if self.controller.install_type != "networkless":
			self.active_selection = self.controller.install_profile.get_cron_daemon_pkg() or self.crons[0]
			self.radio_crons[self.active_selection].set_active(True)

	def next(self):
		if self.controller.install_type == "networkless":
			self.controller.install_profile.set_cron_daemon_pkg(None, self.crons[0], None)
		else:
			self.controller.install_profile.set_cron_daemon_pkg(None, self.active_selection, None)
		progress = ProgressDialog(self.controller, ('install_logging_daemon', 'install_cron_daemon', 'install_filesystem_tools'), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("Bootloader")
		else:
			GLIScreen.progress_callback(self, result, data)
