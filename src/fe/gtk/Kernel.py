# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
from ProgressDialog import *

class Panel(GLIScreen):

	title = "Kernel"
	_helptext = """
<b><u>Kernel</u></b>

There are 2 different methods for getting a kernel for your new install: \
using the kernel/modules from the LiveCD that you are currently booted from \
and building your own. Pick one.

These choices are only available in Standard and Advanced mode.
"""

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		if self.controller.install_type == "networkless":
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>The kernel, initramfs, and modules from the LiveCD will be used</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)
		else:
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>How do you want to get a kernel?</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			hbox = gtk.HBox(False, 0)
			self.radio_livecd_kernel = gtk.RadioButton(None, "Use the kernel, initramfs, and modules from the LiveCD")
			hbox.pack_start(self.radio_livecd_kernel, expand=False, fill=False, padding=20)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			hbox = gtk.HBox(False, 0)
			self.radio_build_kernel = gtk.RadioButton(self.radio_livecd_kernel, "Build your own from sources")
			hbox.pack_start(self.radio_build_kernel, expand=False, fill=False, padding=20)
			vert.pack_start(hbox, expand=False, fill=True, padding=20)

		self.add_content(vert)

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = True
		if self.controller.install_type != "networkless":
			kernel_pkg = self.controller.install_profile.get_kernel_source_pkg()
			if not kernel_pkg or kernel_pkg == "livecd-kernel":
				self.radio_livecd_kernel.set_active(True)
			else:
				self.radio_build_kernel.set_active(True)

	def next(self):
		if self.controller.install_type == "networkless" or self.radio_livecd_kernel.get_active():
			self.controller.install_profile.set_kernel_source_pkg(None, "livecd-kernel", None)
			progress = ProgressDialog(self.controller, ("emerge_kernel_sources", ), self.progress_callback)
			progress.run()
		else:
			self.controller.load_screen("KernelSources")

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("Networking")
		else:
			GLIScreen.progress_callback(self, result, data)
