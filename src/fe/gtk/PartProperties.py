# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk, pango
import PartitionButton
from gettext import gettext as _

class PartProperties(gtk.Window):

	def __init__(self, controller, partition):
		gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)

		self.controller = controller
		self.device = partition['device'].get_device()
		self.part = partition
		self.idx = self.part['device'].get_partition_idx_from_start_end(partition['start'], partition['end'])
		self.minor = self.part['minor']
		self.min_size = self.part['min_size']
		self.max_size = self.part['max_size']
		self.cur_size = self.part['mb']
		self.fstype = self.part['type']
		if self.min_size == -1 or self.max_size == -1:
			self.min_size = self.cur_size
			self.max_size = self.cur_size
		self.supported_filesystems = self.controller.devices[self.device].get_supported_filesystems()

		self.connect("delete_event", self.delete_event)
		self.connect("destroy", self.destroy_event)
		self.set_default_size(400,200)
		if self.fstype == "free":
			self.set_title(_("New partition on ") + self.device)
		else:
			self.set_title(_("Properties for ") + self.part['devnode'])

		self.globalbox = gtk.VBox(False, 0)
		self.globalbox.set_border_width(10)

		self.resize_box = gtk.VBox(False, 0)
		resize_text_box = gtk.HBox(False, 0)
		resize_text_box.pack_start(gtk.Label(_("New size:")), expand=False, fill=False, padding=0)
		self.resize_info_part_size = gtk.Entry(max=9)
		self.resize_info_part_size.set_width_chars(7)
		self.resize_info_part_size.set_text(str(int(self.cur_size)))
		self.resize_info_part_size.connect("insert-text", self.validate_keypress)
		self.resize_info_part_size.connect("focus-out-event", self.update_entries, "part-size")
		resize_text_box.pack_start(self.resize_info_part_size, expand=False, fill=False, padding=6)
		resize_text_box.pack_start(gtk.Label(_("MB")), expand=False, fill=False, padding=0)
		resize_text_box.pack_start(gtk.Label("  "), expand=False, fill=False, padding=20)
		resize_text_box.pack_start(gtk.Label(_("Unalloc. size:")), expand=False, fill=False, padding=6)
		self.resize_info_unalloc_size = gtk.Entry(max=9)
		self.resize_info_unalloc_size.set_width_chars(7)
		self.resize_info_unalloc_size.set_text(str(int(self.max_size - self.cur_size)))
		self.resize_info_unalloc_size.connect("insert-text", self.validate_keypress)
		self.resize_info_unalloc_size.connect("focus-out-event", self.update_entries, "unalloc-size")
		resize_text_box.pack_start(self.resize_info_unalloc_size, expand=False, fill=False, padding=0)
		resize_text_box.pack_start(gtk.Label("MB"), expand=False, fill=False, padding=3)
		self.resize_box.pack_start(resize_text_box, expand=False, fill=False, padding=10)
		self.globalbox.pack_start(self.resize_box, expand=False, fill=False, padding=0)

		self.part_info_box = gtk.HBox(False, 0)
		part_info_table = gtk.Table(6, 2, False)
		part_info_table.set_col_spacings(10)
		part_info_table.set_row_spacings(5)
		info_partition_label = gtk.Label(_("Partition:"))
		info_partition_label.set_alignment(0.0, 0.5)
		part_info_table.attach(info_partition_label, 0, 1, 0, 1)
		self.info_partition = gtk.Label()
		self.info_partition.set_alignment(0.0, 0.5)
		part_info_table.attach(self.info_partition, 1, 2, 0, 1)

		info_partition_type = gtk.Label(_("Type:"))
		info_partition_type.set_alignment(0.0, 0.5)
		part_info_table.attach(info_partition_type, 0, 1, 1, 2)
		self.resize_info_part_type = gtk.combo_box_new_text()
		self.resize_info_part_type.append_text(_("Primary"))
		self.resize_info_part_type.append_text(_("Logical"))
		self.resize_info_part_type.set_active(0)
		part_info_table.attach(self.resize_info_part_type, 1, 2, 1, 2)

		info_partition_fs = gtk.Label(_("Filesystem:"))
		info_partition_fs.set_alignment(0.0, 0.5)
		part_info_table.attach(info_partition_fs, 0, 1, 2, 3)
		self.resize_info_part_filesystem = gtk.combo_box_new_text()
		for fs in self.supported_filesystems:
			self.resize_info_part_filesystem.append_text(fs)
		self.resize_info_part_filesystem.set_active(0)
		part_info_table.attach(self.resize_info_part_filesystem, 1, 2, 2, 3)

		if self.fstype == "free":
			info_partition_mkfsopts = gtk.Label(_("Format options:"))
			info_partition_mkfsopts.set_alignment(0.0, 0.5)
			part_info_table.attach(info_partition_mkfsopts, 0, 1, 3, 4)
			self.resize_info_part_mkfsopts = gtk.Entry(30)
			part_info_table.attach(self.resize_info_part_mkfsopts, 1, 2, 3, 4)

		self.part_info_box.pack_start(part_info_table, expand=False, fill=False, padding=0)
		self.globalbox.pack_start(self.part_info_box, expand=False, fill=False, padding=10)

		bottom_box = gtk.HBox(True, 0)
		ok_button = gtk.Button(_(" OK "))
		ok_button.set_flags(gtk.CAN_DEFAULT)
		ok_button.set_size_request(60, -1)
		ok_button.connect("clicked", self.ok_clicked)
		bottom_box.pack_start(ok_button, expand=False, fill=False, padding=0)
		cancel_button = gtk.Button(_(" Cancel "))
		cancel_button.set_size_request(60, -1)
		cancel_button.connect("clicked", self.cancel_clicked)
		bottom_box.pack_start(cancel_button, expand=False, fill=False, padding=0)
		self.globalbox.pack_end(bottom_box, expand=False, fill=False, padding=0)

		self.add(self.globalbox)
		ok_button.grab_default()
		self.set_modal(True)
		self.set_transient_for(self.controller.controller.window)
		self.show_all()

	def run(self):
		if self.fstype == "free":
			self.info_partition.set_text(self.device + _(" (unallocated)"))
			if self.part['device']._labelinfo['extended']:
				if self.part['device'].get_extended_partition() != -1:
					if self.part.is_logical():
						self.resize_info_part_type.set_active(1)
					else:
						self.resize_info_part_type.set_active(0)
					self.resize_info_part_type.set_sensitive(False)
				else:
					self.resize_info_part_type.set_active(0)
					self.resize_info_part_type.set_sensitive(True)
				self.resize_info_part_filesystem.set_active(0)
				self.resize_info_part_filesystem.set_sensitive(True)
			else:
				self.resize_info_part_type.set_active(0)
				self.resize_info_part_type.set_sensitive(False)
		else:
			self.info_partition.set_text(self.part['devnode'])
			if self.part.is_logical():
				self.resize_info_part_type.set_active(1)
			else:
				self.resize_info_part_type.set_active(0)
			self.resize_info_part_type.set_sensitive(False)
			self.resize_info_part_filesystem.set_sensitive(False)
			for i, fs in enumerate(self.supported_filesystems):
				if fs == self.fstype:
					self.resize_info_part_filesystem.set_active(i)
					break

	def ok_clicked(self, button):
		if not int(self.resize_info_part_size.get_text()):
			msgdlg = gtk.MessageDialog(parent=self.controller.controller.window, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format="You cannot define a 0MB partition.")
			resp = msgdlg.run()
			msgdlg.destroy()
			return
		if self.fstype == "free":
			part_size = int(self.resize_info_part_size.get_text())
			if self.resize_info_part_type.get_active() == 1 and self.controller.devices[self.device].get_extended_partition() == -1: # Logical and no extended partition
				extidx = self.part['device'].add_partition(self.idx, self.max_size, "extended")
				extpart = self.part['device'][extidx]
				for idx, tmppart in enumerate(self.part['device']):
					if tmppart['type'] == "free" and tmppart['start'] >= extpart['start'] and tmppart['end'] <= extpart['end']:
						self.idx = idx
			fstype = self.supported_filesystems[self.resize_info_part_filesystem.get_active()]
			newidx = self.controller.devices[self.device].add_partition(self.idx, part_size, fstype, mkfsopts=self.resize_info_part_mkfsopts.get_text())
			self.controller.draw_part_box()
			self.controller.part_selected(None, self.device, newidx)
		else:
			self.controller.draw_part_box()
			self.controller.part_selected(None, self.device, self.idx)

		self.destroy()

	def cancel_clicked(self, button):
		self.destroy()

	def validate_keypress(self, editable, new_text, new_text_length, position):
		if new_text == ".": return
		try:
			float(new_text)
		except:
			editable.stop_emission("insert-text")

	def update_entries(self, widget, event, which_one):
#		print "update_entries(): which_one=" + which_one
		if which_one == "part-size":
			part_size_mb = long(self.resize_info_part_size.get_text())
#			print "part_size_mb=%s, max_size=%s, min_size=%s" % (str(part_size_mb), str(self.max_size), str(self.min_size))
			if part_size_mb > self.max_size:
				part_size_mb = self.max_size
			elif part_size_mb < self.min_size:
				part_size_mb = self.min_size
			self.resize_info_part_size.set_text(str(part_size_mb))
			part_unalloc_mb = long(self.max_size - part_size_mb)
			self.resize_info_unalloc_size.set_text(str(part_unalloc_mb))
		else:
			part_unalloc_mb = long(self.resize_info_unalloc_size.get_text())
			if part_unalloc_mb > (self.max_size - self.min_size):
				part_unalloc_mb = long(self.max_size - self.min_size)
				self.resize_info_unalloc_size.set_text(str(part_unalloc_mb))
			part_size_mb = long(self.max_size - part_unalloc_mb)
			self.resize_info_part_size.set_text(str(part_size_mb))

	def delete_event(self, widget, event, data=None):
		return False

	def destroy_event(self, widget, data=None):
		return True
