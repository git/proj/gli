# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
import GLIUtility
import URIBrowser
from ProgressDialog import *

class Panel(GLIScreen):

	title = "Kernel Config"
	active_selection = None
	radio_configs = {}
	_helptext = """
<b><u>Kernel Configuration</u></b>

There are a few different ways you can configure your new kernel. The easiest \
method is to just use the default configuration for your kernel. The other \
options are to configure the kernel yourself using the kernel's menuconfig process \
or to specify a URI pointing to a pre-made kernel .config file.

These options are only available in Advanced mode.
"""

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		if self.controller.install_type == "standard":
			hbox = gtk.HBox(False, 0)
			label = gtk.Label()
			label_markup = '<b>Your kernel will be built with the default configuration.</b>'
			label.set_markup(label_markup)
			label.set_line_wrap(True)
			hbox.pack_start(label, expand=False, fill=False, padding=5)
			vert.pack_start(hbox, expand=False, fill=False, padding=10)
		else:
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Choose your kernel configuration method</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			hbox = gtk.HBox(False, 0)
			self.radio_configs['auto'] = gtk.RadioButton(None, "Use default configuration")
			self.radio_configs['auto'].set_name("auto")
			self.radio_configs['auto'].connect("toggled", self.config_selected, "auto")
			hbox.pack_start(self.radio_configs['auto'], expand=False, fill=False, padding=20)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			hbox = gtk.HBox(False, 0)
			self.radio_configs['menuconfig'] = gtk.RadioButton(self.radio_configs['auto'], "Use the kernel's menuconfig process")
			self.radio_configs['menuconfig'].set_name("menuconfig")
			self.radio_configs['menuconfig'].connect("toggled", self.config_selected, "menuconfig")
			hbox.pack_start(self.radio_configs['menuconfig'], expand=False, fill=False, padding=20)
			vert.pack_start(hbox, expand=False, fill=True, padding=20)

			hbox = gtk.HBox(False, 0)
			self.radio_configs['configuri'] = gtk.RadioButton(self.radio_configs['auto'], "Specify a pre-made kernel .config")
			self.radio_configs['configuri'].set_name("configuri")
			self.radio_configs['configuri'].connect("toggled", self.config_selected, "configuri")
			hbox.pack_start(self.radio_configs['configuri'], expand=False, fill=False, padding=20)
			vert.pack_start(hbox, expand=False, fill=True, padding=20)

			hbox = gtk.HBox(False, 0)
			hbox.pack_start(gtk.Label(" "), expand=False, fill=False, padding=15)
			hbox.pack_start(gtk.Label("URI:"), expand=False, fill=False, padding=5)
			self.entry_kernel_config_uri = gtk.Entry()
			self.entry_kernel_config_uri.set_width_chars(50)
			self.entry_kernel_config_uri.set_sensitive(False)
			hbox.pack_start(self.entry_kernel_config_uri, expand=False, fill=False, padding=0)
			self.browse_uri = gtk.Button("Browse")
			self.browse_uri.connect("clicked", self.browse_uri_clicked)
			self.browse_uri.set_sensitive(False)
			hbox.pack_start(self.browse_uri, expand=False, fill=False, padding=5)
			vert.pack_start(hbox, expand=False, fill=False, padding=0)

		self.add_content(vert)

	def browse_uri_clicked(self, widget):
		uribrowser = URIBrowser.URIBrowser(self, self.entry_kernel_config_uri)
		uribrowser.run(self.entry_kernel_config_uri.get_text())

	def config_selected(self, widget, data=None):
		self.active_selection = data
		if data == "configuri":
			self.entry_kernel_config_uri.set_sensitive(True)
			self.browse_uri.set_sensitive(True)
		else:
			self.entry_kernel_config_uri.set_sensitive(False)
			self.browse_uri.set_sensitive(False)

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = True
		self.controller.SHOW_BUTTON_FORWARD = True
		if self.controller.install_type != "standard":
			kernel_config_uri = self.controller.install_profile.get_kernel_config_uri()
			if kernel_config_uri:
				if kernel_config_uri == "file://%s/usr/src/linux/.config" % self.controller.install_profile.get_root_mount_point():
					self.radio_configs['menuconfig'].set_active(True)
				else:
					self.entry_kernel_config_uri.set_text(kernel_config_uri)
#			if self.controller.install_profile.get_kernel_source_pkg() != "gentoo-sources":
#				self.radio_configs['auto'].set_sensitive(False)
#				self.radio_configs['menuconfig'].set_active(True)

	def next(self):
		self.controller.install_profile.set_kernel_build_method(None, "genkernel", None)
		if self.controller.install_type != "standard":
			if self.active_selection == "configuri":
				if not self.entry_kernel_config_uri.get_text():
					msgdlg = gtk.MessageDialog(parent=self.controller.window, type=gtk.MESSAGE_WARNING, buttons=gtk.BUTTONS_OK, message_format="You did not enter a kernel configuration URI. You cannot continue until you enter a kernel configuration URI or choose a different configuration method.")
					msgdlg.run()
					msgdlg.destroy()
					return
				elif not GLIUtility.validate_uri(self.entry_kernel_config_uri.get_text()):
					msgdlg = gtk.MessageDialog(parent=self.controller.window, type=gtk.MESSAGE_WARNING, buttons=gtk.BUTTONS_OK, message_format="The kernel configuration URI you entered does not exist or is not a valid URI. You cannot continue unless you enter a valid URI or choose another configuration method.")
					msgdlg.run()
					msgdlg.destroy()
					return
			elif self.active_selection == "menuconfig":
				fo = open(self.controller.install_profile.get_root_mount_point() + "/var/tmp/menuconfig.sh", "w")
				fo.write("#!/bin/bash -l\ncd /usr/src/linux\nmake menuconfig")
				fo.close()
				while True:
					GLIUtility.spawn("xterm -e 'chroot %s /var/tmp/menuconfig.sh'" % self.controller.install_profile.get_root_mount_point())
					msgdlg = gtk.MessageDialog(parent=self.controller.window, type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO, message_format="Are you finished configuring your kernel?")
					resp = msgdlg.run()
					msgdlg.destroy()
					if resp == gtk.RESPONSE_YES:
						self.controller.install_profile.set_kernel_config_uri(None, "file://%s/usr/src/linux/.config", None)
						GLIUtility.spawn("rm %s/var/tmp/spawn.sh" % self.controller.install_profile.get_root_mount_point())
						break
		progress = ProgressDialog(self.controller, ("build_kernel", ), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("Networking")
		else:
			GLIScreen.progress_callback(self, result, data)
