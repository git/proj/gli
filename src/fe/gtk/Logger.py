# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
from ProgressDialog import *

class Panel(GLIScreen):

	title = "Logging Daemon"
	active_selection = None
	radio_loggers = {}
	_helptext = """
<b><u>Logging Daemon</u></b>

Pick a logging daemon. The most common choice is syslog-ng. This option is not \
available in Networkless mode.
"""
	loggers = [ "syslog-ng", "metalog", "sysklogd" ]

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		if self.controller.install_type == "networkless":
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Your logger will be %s</b>' % self.loggers[0])
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)
		else:
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Choose your logger</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			for logger in self.loggers:
				hbox = gtk.HBox(False, 0)
				if logger == self.loggers[0]:
					self.radio_loggers[logger] = gtk.RadioButton(None, logger)
				else:
					self.radio_loggers[logger] = gtk.RadioButton(self.radio_loggers[self.loggers[0]], logger)
				self.radio_loggers[logger].set_name(logger)
				self.radio_loggers[logger].connect("toggled", self.logger_selected, logger)
				hbox.pack_start(self.radio_loggers[logger], expand=False, fill=False, padding=20)
				vert.pack_start(hbox, expand=False, fill=False, padding=20)

		self.add_content(vert)
		self.boot_devices = None

	def logger_selected(self, widget, data=None):
		self.active_selection = data

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = True
		if self.controller.install_type != "networkless":
			self.active_selection = self.controller.install_profile.get_logging_daemon_pkg() or self.loggers[0]
			self.radio_loggers[self.active_selection].set_active(True)

	def next(self):
		if self.controller.install_type == "networkless":
			self.controller.install_profile.set_logging_daemon_pkg(None, self.loggers[0], None)
		else:
			self.controller.install_profile.set_logging_daemon_pkg(None, self.active_selection, None)
		self.controller.load_screen("CronDaemon")
#		progress = ProgressDialog(self.controller, ("install_logger", ), self.progress_callback)
#		progress.run()

#	def progress_callback(self, result, data=None):
#		if result == PROGRESS_DONE:
#			self.controller.load_screen("Timezone")
#		else:
#			GLIScreen.progress_callback(self, result, data)
