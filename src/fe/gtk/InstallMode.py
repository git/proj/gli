# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *

class Panel(GLIScreen):

	title = _("Welcome to the Gentoo Linux Installer")
	_helptext = _("""
<b><u>Install Mode</u></b>

Welcome to the GTK+ front-end for the Gentoo Linux Installer. It is highly \
recommended that you have gone through the manual install process a time or \
two, or at least read through the install guide.

There are 3 install modes you can choose from: Networkless, Standard, and \
Advanced. The "Networkless" mode is used for installs where you have no access \
to the internet, or you just want a fast install using what's available on the \
LiveCD.  The "Standard" mode is used for networked installs. It allows you \
some flexibility, but it makes safe assumptions on some advanced options. The \
"Advanced" mode is for people who want to play around with more settings. If \
you break your install in this mode, don't come crying to us.
""")

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 10)
		vert.set_border_width(10)

		hbox = gtk.HBox(False)
		label = gtk.Label()
		label.set_markup(_('<b>Choose your install mode</b>'))
		hbox.pack_start(label, expand=False, fill=False, padding=0)
		vert.pack_start(hbox, expand=False, fill=False, padding=15)

		hbox = gtk.HBox(False)
		self.install_type_standard = gtk.RadioButton(label=_("Standard"))
		hbox.pack_start(self.install_type_standard, expand=False, fill=False, padding=20)
		vert.pack_start(hbox, expand=False, fill=False, padding=5)
		hbox = gtk.HBox(False)
		self.install_type_networkless = gtk.RadioButton(group=self.install_type_standard, label=_("Networkless"))
		hbox.pack_start(self.install_type_networkless, expand=False, fill=False, padding=20)
		vert.pack_start(hbox, expand=False, fill=False, padding=5)
		hbox = gtk.HBox(False)
		self.install_type_advanced = gtk.RadioButton(group=self.install_type_networkless, label=_("Advanced"))
		hbox.pack_start(self.install_type_advanced, expand=False, fill=False, padding=20)
		vert.pack_start(hbox, expand=False, fill=False, padding=5)

		self.add_content(vert)

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = True

	def next(self):
		if self.install_type_networkless.get_active():
			self.controller.install_type = "networkless"
		elif self.install_type_standard.get_active():
			self.controller.install_type = "standard"
		elif self.install_type_advanced.get_active():
			self.controller.install_type = "advanced"
		self.controller.load_screen("Partition")
