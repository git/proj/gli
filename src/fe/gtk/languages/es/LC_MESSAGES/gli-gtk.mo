��    �      <    \      x  ~  y  v   �  A  o  �  �  �  �    T  G  [  a  �  �   &  `  �&  W  �'  �  H)  )  +/     U0     h0     q0     u0     �0     �0     �0     �0     �0  	   �0  	   �0     �0     �0     �0     1     1     (1     41     <1  	   H1     R1     ^1     ~1     �1      �1     �1     �1     �1     2     2     .2  ,   72  +   d2  1   �2     �2      �2     �2  7   3  !   V3  
   x3     �3  �   �3      4     :4     U4  r   p4  "   �4  )   5     05     85  %   X5     ~5  %   �5  #   �5     �5     �5  &   �5     6  $   56     Z6     h6     ~6     �6     �6     �6     �6  
   �6     �6     �6     �6     �6  <   7  B   J7     �7  $   �7     �7  o   �7     =8     C8  8   Y8     �8     �8  	   �8     �8     �8     �8     �8     9  .   +9     Z9     a9     v9     ~9  $   �9  
   �9     �9  $   �9  #   �9     :     &:     5:     U:     q:     �:     �:     �:     �:  /   �:     �:     �:     ;     ;     /;     J;     Y;  (   f;     �;     �;     �;     �;     �;     �;     �;     	<  4   <     Q<  
   W<     b<     o<     x<     �<     �<     �<     �<     �<     �<  "   �<     =  "   $=  /   G=     w=     =     �=  !   �=     �=     �=     �=  g   �=  5   b>     �>     �>     �>     �>     �>     �>  '   �>  T   ?  f   `?     �?     �?     �?     �?     �?     �?     @     @     ,@     9@     @@     I@  %   V@     |@  
   �@  �   �@  2   A  �   DA  1   �A      B  +   <B  )   hB     �B     �B  9  �B    �C  �   [E  S  �E  0  :G  �  kJ    �M  u  P  �  �S  �   a[  l  
\  N  w]  �  �^  "  qe     �f     �f     �f     �f     �f     �f     �f     �f     g  	    g  	   *g     4g     ;g     Hg     [g     mg     zg  
   �g  	   �g     �g     �g  )   �g  +   �g  "   h  &   1h     Xh     ih     |h     �h     �h     �h  4   �h  5   �h  :   4i     oi     �i  (   �i  6   �i  -   j     4j     Ij  p   ^j  '   �j     �j      k  t   4k  +   �k  *   �k  
    l     l  /   l     Hl  '   dl     �l  	   �l  	   �l  %   �l  &   �l  *   m     6m     Dm     Zm     cm     rm     m     �m     �m     �m     �m     �m  %   �m  H   n  A   \n     �n  (   �n     �n  �   �n     io     oo  (   �o  	   �o     �o  
   �o     �o  &   �o  "   p     .p     Jp  4   ap     �p     �p     �p     �p  /   �p      q     q  1    q  3   Rq     �q     �q     �q  /   �q      r     r     0r     Br     Jr  '   Wr     r     �r     �r     �r  %   �r     �r     s  (   s  0   Fs  #   ws     �s     �s     �s     �s      �s     �s  .   t     :t     @t     Lt     Zt     ft     �t  $   �t     �t     �t     �t     �t  %   u     ,u  -   4u  9   bu     �u     �u     �u  '   �u      �u      v     &v     2v  =   �v     �v  	   �v  	   w     w     #w  "   )w  ,   Lw  �   yw  v   �w     sx     �x     �x  
   �x     �x  !   �x  '   �x     �x     y     y     y     -y  )   ?y     iy     vy  �   �y  6   	z  �   @z  3   {  %   L{  7   r{  9   �{  	   �{  	   �{     �   j   �   �          �   �   v      �           P      >      ?   �   e   {      a                      B      �   �   *   
   �   p   �   �   f      �       9          �   `   \   �          �   �   F          �   �           �   t           ]   3   5   �   %   z   ,      �   |   L              �       )   �   �       �   X   C       d   �   �   (      �       }   +       I       i       =   �   �   �       m   q       x          n   �      	   �   �   �   7   �   �       2                           �      &   ;   :   �       �   �   y   [       g      V   ~      Y   �   8           J      !       u   G      -       �           �   ^   T   H       �           4   D   r   k   �      �          �   $   "   �       .       �       �   �           �           h   �   W   �   �   �              l   �   �   @               /   1       R   E          #   _      �   S   6          �   A   c   M       �   0   N           U   w       '   �       Q   K       <   O      �      b   �   s   o       �   �   �   Z    
<b><u>Extra Packages</u></b>

All of the packages listed on the right are available for the installer to install directly from the LiveCD (including dependencies) without access to the internet.

If you choose a graphical desktop such as gnome, kde, or fluxbox, be sure to also select xorg-x11 from the list. Otherwise, you will not have a fully functioning graphical environment.
 
<b><u>Install Complete!</u></b>

Your install has finished. Click the Exit button, restart your computer, and enjoy!
 
<b><u>Install Failed</u></b>

Your install has failed for one of numerous reasons. You can find the error in the logfile at /var/log/install.log.failed. Once you determine that the error was not caused by you, please file a bug at http://bugs.gentoo.org/ in the Gentoo Release Media product and the Installer component.
 
<b><u>Install Mode</u></b>

Welcome to the GTK+ front-end for the Gentoo Linux Installer. It is highly recommended that you have gone through the manual install process a time or two, or at least read through the install guide.

There are 3 install modes you can choose from: Networkless, Standard, and Advanced. The "Networkless" mode is used for installs where you have no access to the internet, or you just want a fast install using what's available on the LiveCD.  The "Standard" mode is used for networked installs. It allows you some flexibility, but it makes safe assumptions on some advanced options. The "Advanced" mode is for people who want to play around with more settings. If you break your install in this mode, don't come crying to us.
 
<b><u>Local Mounts</u></b>

Here, you can add partitions and special devices to mount during (and after) the install. All mountpoints are relative to /mnt/gentoo during the install.

To start, click the button labeled 'Add'. Enter a device name or select if from the list. If you entered a device name yourself, you'll need to fill in the type in the next field. Then enter the mountpoint and mount options (defaults to "defaults"). Click 'Update' to save the new mount.

To edit a mount that you've already added to the list list, select it by clicking on it. Edit any of the fields below and click the 'Update' button. You can remove it from the list by clicking the 'Delete' button.
 
<b><u>Networking</u></b>

All detected interfaces should show up in the list, but you also have the option to type in your own interface. Once you select an interface, select DHCP or Static Configuration.  Then once you have set your network settings make sure to click Save to add the interface to the list.

Wireless support currently is unavailable, but coming soon!  We even have the boxes for it all ready to go.

Don't forget to set a hostname and domain name in the "Hostname / Proxy Information / Other" tab!
 
<b><u>Other Settings</u></b>

Display Manager:
If you installed gnome, choose gdm. If you installed kde, choose kdm. If you installed anything else specified in XSession, choose xdm.

Console Font:
You probably don't want to mess with this.

Extended Keymaps:
You probably don't want to mess with this.

Windowkeys:
If installing on x86 you are safe with Yes, otherwise you'll probably want to say No.

Keymap:
This defaults to "us" if not set (recommended).  If you don't want an English keymap, choose it from the list.

XSession:
Choose this only if you didn't choose gdm or kdm from the Display Manager list.

Clock:
If you chose a local timezone, you'll want to choose "local" for the clock setting. Otherwise if you chose UTC in the Timezone screen, choose UTC here.

Default Editor:
Pick one.  Nano is the default and recommended.
 
<b><u>Partitioning</u></b>

The choices you make on this screen are very important. There are three ways to do your partitioning.

The first method is the Recommended Layout. If you really have no idea how you should lay out your partitions, this is probably the best option. You will need at least 4GB of disk space to use this option. If you have any existing partitions, they will be deleted. Three partitions will be created: /boot (100MB), swap (calculated based on physical memory, up to 512MB), and / (remaining concurrent space).

The second method is the old fashioned one: doing it yourself. The partition edit is fairly straightforward (although, not <i>that</i> straightforward as this help would not need to exist).

The third method is doing it completely "by hand". You can open up a terminal and use <i>fdisk</i>, <i>cfdisk</i>, or some other partitioning program. You will need to create filesystems on all partitions that you create.

The currently active disk is represented by the bar near the top of the screen. If you have more than one disk present in your system, you can change the active disk by choosing another disk from the drop down box labeled 'Devices'. Along the bottom of the screen, there is a color key for the disk representation above.

To view an existing partition's properties, you must select it by clicking it in the bar above. You will get a brief overview of the partition below the bar. To delete it, click the 'Delete' button. You will be asked for confirmation before the partition is removed.

To create a new partition, select some unallocated space in the bar at the top. For a partition for use by Linux, you will want to use ext2, ext3, reiserfs, jfs, or xfs. Ext3 is the recommended type.

Keep in mind that all changes are committed to disk immediately. If you just click the Next button, your partition table will remain untouched.
 
<b><u>Root Password</u></b>

Enter the password for the root user in your new install. Enter it again to confirm it (to prevent typos).
 
<b><u>Startup Services</u></b>

On this screen, you can select services that you would like to startup at boot. Common choices are sshd (remote access) and xdm (graphical login... choose this for kdm, gdm, and entrance, as well). Only services that are provided by a package you already have installed and are not already in a runlevel are displayed.
 
<b><u>Timezone</u></b>

Pick your timezone, or pick UTC. If you dual-boot with Windows you'll want to choose your local timezone. If your BIOS clock is set to local time you'll also want to choose your local timezone.

If you choose a local timezone, you'll want to choose "local" for the clock setting later on in the Other Settings screen.
 
<b><u>Users</u></b>

Working as root on a Unix/Linux system is dangerous and should be avoided as much as possible. Therefore it is strongly recommended to add a user for day-to-day use.

Enter the username and password in respective boxes.  Make sure to type your password carefully, it is not verified. All other fields are optional, but setting groups is highly recommended.

The groups the user is member of define what activities the user can perform. The following table lists a number of important groups you might wish to use: 
<u>Group</u> 		<u>Description</u>
audio 		be able to access the audio devices
cdrom 		be able to directly access optical devices
floppy 		be able to directly access floppy devices
games 		be able to play games
portage 	be able to use emerge --pretend as a normal user
usb 		be able to access USB devices
plugdev 	Be able to mount and use pluggable devices such as cameras and USB sticks
video 		be able to access video capturing hardware and doing hardware acceleration
wheel 		be able to use su

Enter them in a comma-separated list in the groups box.

Optinally you may also specify the user's shell.  The default is /bin/bash.  If you want to disable the user from logging in you can set it to /bin/false. You can also specify the user's home directory (default is /home/username), userid (default is the next available ID), and a comment describing the user.

Make sure to click Accept Changes to save the changes to your user.  They will then show up in the list.
 
This is where you emerge extra packages that your system may need. Packages that
are fetch-restricted or require you to accept licenses (e.g. many
big games) will cause your install to fail. Add additional packages with
caution. These trouble packages can be installed manually after you reboot.
  Clear partitions   Delete   MB  Recommended layout   _Add   _Broadcast:   _Configuration:   _DHCP Options:   _DNS Domain Name:   _Delete   _ESSID:   _Exit   _Gateway:   _Hostname:   _IP Address:   _Interface:   _Netmask:   _Next   _Previous   _Update   _View Log  <b>Choose your install mode</b> <b>Enter a root password</b> <b>Your install has failed.</b> <b>Your install is complete!</b> Accept Changes Activating swap on  Add additional users. Add user Add/Edit a user Advanced An error occured loading the install profile An error occured saving the install profile An error occurred retrieving hardware information An internal error occurred! Are you sure you want to delete  Are you sure you want to exit? Are you sure you wish to clear the partition table for  Automatically partition the drive Broadcast  Building kernel Choose what window manager you want to start default with X if run with xdm, startx, or xinit. (common options are Gnome or Xsession) Choose your default console font Choose your default editor Choose your desired keymap Choose your display manager for Xorg-x11 (note you must make sure that package also gets installed for it to work) Cleaning up after a failed install Cleanup and unmounting local filesystems. Comment Common web server (version 1.x) Compiling kernel.  Please be patient! Configure /etc/make.conf Configuring and installing bootloader Configuring post-install networking Confirm Copying  Copying kernel, initramfs, and modules Could not find the log file Creating VDB entry for livecd-kernel DHCP Options  Deactivating swap on  Delete Delete user Description Determining files to copy Device Device     Device Information Device: Devices: Do you need any extra packages? Do you really want to cancel before the install is complete? Do you want the installer to clean up after itself before exiting? Done Done copying livecd-kernel to chroot Emerge kernel sources Enter a space separated list of extra packages to install on the system ( in addition to those checked above ): Error Error saving packages Exception received while loading partitions from device  Extended Filesystem: Finishing Gateway       Gathering portage configuration Generating module dependencies Generating mount list Gentoo Linux Installer Gentoo Linux Installer - Installation Progress Groups Hardware Information HomeDir HomeDir  Hostname / Proxy Information / Other IP Address Install distcc Install profile loaded successfully! Install profile saved successfully! Installing Cron daemon Installing MTA Installing additional packages. Installing filesystem tools Installing system logger Invalid Entry Local Mounts Logical Malformed IP Malformed IP address in one of your interfaces! Missing information Mount Options Mount Point Mount local partitions Mount network (NFS) shares Mount options: Mount point: Mounting %(partition)s at %(mountpoint)s Moving compile output logfile Moving install logfile N/A Netmask       Networking Settings Networkless No Device Selected No Ethernet Device No information was found in dmesg about your device. Other Partition: Partitioning Password Performing 'emerge system' Performing bootstrap Please enter a device! Preparing chroot Primary Reset loaded password Root Password Running custom post-install script Save Select the install profile to load Select the location to save the install profile Service Set the root password Setting timezone Setting up and running bootloader Setting up services for startup Shell Shell       Should CLOCK be set to UTC or local? Unless you set your timezone to UTC you will want to choose local. Should we first load the 'windowkeys' console keymap? Size: Standard Static Step description here Step: Syncing the Portage tree The passwords you entered do not match! This sets the maps to load for extended keyboards. Most users will leave this as is. This will clear your drive and apply a recommended partition layout. Are you sure you want to do this? Timezone Type Type: Unallocated Unmounting  Unpack stage tarball Updating config files User Settings User screen! UserID Username Username     Welcome to the Gentoo Linux Installer Wireless Working... You cannot create more than 4 primary partitions. If you need more partitions, delete one or more and create logical partitions. You didn't set your hostname and/or dnsdomainname! You have a filesystem mounted on %(drive)s. Please unmount before performing any operations on this device. Failure to do so could cause data loss. You have been warned. You have not configured any interfaces. Continue? You must enter a mountpoint You must enter a value for the device field You must specify a mount at / to continue _Cancel static Project-Id-Version: gli-gtk 2007.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-12-18 12:14-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
 
<b><u>Paquetes Adicionales</u></b>

Los paquetes en la lista de la derecha están disponibles también para ser instalados desde el LiveCD, incluyendo con sus dependencias, sin tener acceso a internet.

Si elije instalar un escritorio gráfico como Gnome, KDE o Fluxbox, debe elegir también xorg-x11 de la lista. De otra manera, no tendrá un entorno gráfico totalmente funcional
 
<b><u>¡Instalación Completa!</u></b>

La instalación ha terminado. Salga del programa, reinicie su computadora, y disfrute de Gentoo!
 
<b><u>Instalación Fallida</u></b>

Su instalación puede haber fallado por numerosas razones. Puede encontrar el error en la bitácora /var/log/install.log.failed. Una vez determinado que el problema no fue causado por Ud., por favor abra una incidencia en http://bugs.gentoo.org/ en los componentes Gentoo Release Media and Installer. 
 
<b><u>Modo de Instalación</u></b>

Bienvenidos al front-end GTK+ para el Instalador de Gentoo Linux. Es altamente recomendable que haya pasado por el proceso manual de instalación una o dos veces o que al menos haya leído la guía de instalación.

Existen 3 maneras que puede seleccionar para instalar: Sin Red, Estándar y Avanzada. La manera "Sin Red" es para instalaciones donde no se tiene acceso a internet o si desea una instalación rápida utilizando lo que se tiene en el LiveCD. La manera "Estándar" es utilizada para instalaciones con red. Permite alguna flexibilidad pero asume, de manera segura, sobre ciertas opciones avanzadas. La manera "Avanzada" es para personas que deseen jugar con muchas mas opciones. Si usted estropea su instalación en este modo de instalación, no venga a llorarnos. 
 
<b><u>Puntos de Montaje Locales</u></b>

Aquí puede agregar particiones y dispositivos especiales para que sean montados durante (y luego) de la instalación. Todos los puntos de montaje serán relativos a /mnt/gentoo durante el proceso de instalación.
 
Para comenzar, presione el botón 'Agregar'. Introduzca un nombre de dispositivo desde la lista. Si introdujo un nombre de dispositivo que no se encuentra en la lista, debe rellenar el tipo de dispositivo en el siguiente campo. Luego, introduzca el punto y las opciones de montaje (si quiere opciones por defecto, introduzca "defaults"). Presione el botón 'Actualizar' para guardar el nuevo punto de montaje. 

Para editar un punto de montaje que ya ha agregado a la lista, selecciónelo haciendo clic sobre él. Edite los campos necesarios y luego presione el botón 'Actualizar'. Puede eliminarlo de la lista presionando el botón 'Eliminar'.
 
<b><u>Red</u></b>

Todas las interfaces detectadas se deberían ver en la lista, pero también tiene la opción de señalar su propia interfaz. Una vez seleccionada, puede elegir DHCP o una Configuración Estática. Después de haber configurado la red asegúrese de presionar 'Guardar' para añadir la interfaz a la lista

El soporte Wireless no está disponible ahora, pero lo estará pronto! Aún tenemos las cajas listas para comenzar.

No olvide señalar un hostname y nombre de dominio en la pestaña "Información de Hostname/Proxy "
 
<b><u>Otras Opciones</u></b>

Manejador de Entornos:
Si instaló Gnome, elija GDM. Si instaló KDE, elija KDM. Si instaló cualquier otro entorno especificado en XSession, elija XDM

Fuente de Cónsola:
Probablemente no desee modificar esto.

Mapas de Teclado Extendidos:
Probablemente no desee modificar esto.

Windowkeys:
Si está instalando en un x86 puede elejir 'Si' con confianza, de otra manera quizá deba elejir 'No'.

Mapa de Teclado:
Por defecto se usa "us" como Mapa de Teclado. Si no desea usar un Mapa Inglés, elija el de su preferencia de la lista.

XSession:
Elija esta opción si usted no escogió GDM o KDM de la lista de manejador de entornos.

Reloj:
Si eligió un Huso Horario propio, debería elegir "local" como opción. Si eligió UTC como Huso Horario, debería seleccionar UTC aquí.

Editor por Defecto:
Elija el de su perferencia. Nano es el recomendado.
 
<b><u>Particionando</u></b>

Las elecciones que haga en esta pantalla son muy importantes. Existen tres maneras de particionar.

El primer método es la Configuración Recomendada. Si realmente no tiene idea cómo debe disponer las particiones, ésta es el mejor método. Necesita al menos 4GB de espacio en disco para utilizar esta opción. Si el disco ya se encuentra particionado, las particiones serán eliminadas. Las siguientes particiones serán. creadas: /boot (100MB), swap (calculada en base a la memoria física, hasta 512MB),  y / (con el espacio restante).

El segundo método es el tradicional: hágalo Ud. mismo. La edición de particiones es sencilla (sin embargo, no <i>tan</i> sencilla ya que esta ayuda existe).
 
El tercer método es hacerlo completamente a "mano". Puede abrir un terminal y utilizar <i>fdisk</i>, <i>cfdisk</i>, o cualquier otro editor de particiones. Deberá crear sistemas de archivo en todas las particiones que cree.

El disco activo está representado por la barra cerca del tope de la pantalla. Si tiene más de un disco en su sistema, puede cambiar el disco activo del seleccionador  marcado como 'Dispositivos'. A lo largo de la parte inferior de la pantalla se encuentra la leyenda para la representación de la barra.

Para ver las propiedades de una partición, debe seleccionarla haciendo clic en la barra de representación del disco. Obtendrá con esto una breve descripción de la partición, por debajo de la barra. Para eliminarla, haga clic en el botón 'Eliminar'. Debe confirmar la operación antes de que la partición sea eliminada.

Para crear una nueva partición, seleccione el espacio disponible en la barra de representación. Para crear una partición para Linux, querrá utilizar ext2, ext3, reiserfs, jfs o xfs. Ext3 es el tipo recomendado.

Tenga en cuenta que todos los cambios son aplicados al disco de inmediato. Si presiona el botón de 'Siguiente' sin realizar modificaciones, su tabla de particiones quedará quedará sin cambios.
 
<b><u>Contraseña de Root</u></b>

Introduzca la contraseña para el usuario root de su nueva instalación. Introdúzcala de nuevo  para confirmarla y evitar errores.
 
<b><u>Servicios de Arranque</u></b>

En esta pantalla puede seleccionar los servicios que desee para que arranquen al iniciar el sistema. Algunas opciones comúnes son sshd(acceso remoto) y xdm (login gráfico... puede seleccione para esto kdm, gdm o entrance). Sólo aquellos servicios que han sido instalados y no se encuentran en un runlevel serán mostrados.
 
<b><u>Huso Horario</u></b>

Elija su Huso horario o UTC. Si tiene un arranque dual con Windows debería elejir su huso horario local, al igual que si el reloj de su BIOS está configurado a la hora local.

Si elije un Huso Horario local, debería elejor "local" como opción parael reloj más tarde en la pantalla de Otras Opciones.
 
<b><u>Usuarios</u></b>

Trabajar como root en un sistema Linux/Unix es muy peligroso y debe ser evitado siempre. Por lo tanto, es muy recomendable añadir un usuario para el uso cotidiano del sistema.

Introduzca el nombre de usuario y la contraseá en los espacios respectivos. Asegúrese de introducir su contraseña con cuidado, ésta no se verifica. Las demás son opcionales, pero la configuración de los grupos es altamente recomendada.

Los grupos en los cuales el usuario es miembro definen las tareas que éste puede ejecutar. La siguiente tabla muestra un número de grupos importantes que quizá desee usar para su usuario: 
<u>Grupo</u> 		<u>Descripción</u>
audio 		acceso a los dispositivos de audio
cdrom 		acceso directo a los dispositivos ópticos
floppy 		acceso directo a las unidades de disquette
games 		capacidad para ejecutar juegos
portage 	capacidad para usar emerge --pretend como usuario normal
usb 		capcidad de montar dispositivos USB
plugdev 	capacidad de montar dispositivos enchufables como cámaras y memorias USB
video 		acceso a la captura de video y la aceleración de video por hardware
wheel 		capacidad de usar su

Introduzca los grupos en una lista separada con comas en el campo grupos.

Opcionalmente puede señalar el shell del usuario. El shell por defecto es /bin/bash.  Si desea desactivar al usuario para ingresar al sistema, puede señalarlo como /bin/false.También puede señalar el directorio home del usuario (/home/nombredeusuario)el userid (por defecto se toma el siguiente ID disponible), y una descripción corta del usuario.

Asegúrese de pulsar 'Aceptar Cambios' para guardar las opciones del usuario. Éstas serán mostradas luego en la lista.
 
Ahora se instalarán los paquetes adicionales que su sistema necesite.Aquellos
restringidos o con licencias que se deban aceptar, ocasionarán que la
instalación falle. Tenga cuidado al elegir los paquetes adicionales.
Esos paquetes problemáticos se pueden instalar después del reboot.
 Limpiar Particiones Eliminar  MB  Esquema Recomendado _Agregar  _Broadcast:   _Configuración:   Opciones _DHCP:   Nombre de Dominio _DNS:  _Eliminar  _ESSID:  _Salir  _Pasarela:   Nombre de _Host:   Dirección _IP:   _Interfaz:   Máscara de _Red:  Siguient_e _Anterior A_ctualizar _Ver Log <b>Seleccione su modo de instalación</b> <b>Introduzca una contraseña para root</b> <b>Su instalación ha fallado.</b> <b>¡Su Instalación ha terminado!</b> Acceptar Cambios Activando swap en  Agregar usuarios adicionales Añadir Usuario Añadir/Editar usuario Avanzada Ocurrió un error cargando el perfil de instalación Ocurrió un error guardando el perfil de instalación Ocurrió un error recolectando la información de Hardware Ha ocurrido un error interno!  Seguro quiere eliminar  ¿Seguro que desea salir del instalador? Seguro quiere limpiar la tabla de particionamiento de  Particionando el dispositivo automáticamente Dirección Broadcast Compilando el kernel Elija el manejador de ventanas que desea iniciar al ejecutar xdm, startx o xinit. (por ejemplo Gnome o XSession) Elija su Fuente de Cónsola por
defecto Elija su Editor por defecto Elija el Mapa de Teclado deseado Elija el manejador de entornos para
Xorg-x11 (debe estar seguro de que
el paquete está instalado para que
funcione) Limpiando luego de una instalación fallida Limpiando y desmontando volúmenes locales Comentario Servidor Web Compilando el kernel. ¡Por favor sea paciente! Configurando /etc/make.conf Configurando e instalando el bootloader Configurando servicios de red Confirmar Copiando  Copiando kernel, initramfs y módulos No se pudo encontrar el archivo de log Creando registro VDB para el livecd-kernel Opciones DHCP Desactivando swap en  Eliminar Borrar Usuario Descripción Determinando archivos a copiar Dispositivo Dispositivo Información del Dispositivo Dispositivo: Dispositivos ¿Necesitas algún paquete adicional? ¿Realmente desea cancelar antes de terminar el proceso de instalación? ¿Quiere que el instalador limpie lo que ha hecho antes de salir? Listo! Copiado el livecd-kernel al chroot listo Emerge fuentes del kernel Introduzca una lista separada por espacios con los paquetes adicionales que desea instalar (que no estén en la lista anterior): Error Error guardando paquetes Error cargando particiones de la unidad  Extendida Sistema de Archivos: Terminando Pasarela Recolectando configuración de portage Generando dependencias de módulos Generando lista de montajes Gentoo Linux Installer Gentoo Linux Installer - Progreso de la Instalación Grupos Información de Hardware Carpeta Home Carpeta Home Nombre de Host / Información del Proxy / Otros Dirección IP Instalando distcc Perfil de instalación cargado satisfactoriamente Perfil de instalación guardado satisfactoriamente! Instalando el demonio Cron Instalando MTA Instalando paquetes adicionales Instalando herramientas del sistema de archivos Instalando logger del sistema Entrada inválida Puntos de Montaje Lógica IP inválida IP inválida en uno de sus dispositivos Falta Información Opciones de Mount Punto de Montaje Montando particiones locales Montando volúmenes compartidos (NFS) Opciones de Mount: Punto de Montaje: Montando %(partition)s en %(mountpoint)s Moviendo el logfile de salida de la compilación Moviendo el logfile de instalación N/A Máscara de Red Configuración de Red Sin Red No hay dispositivo seleccionados No hay dispositivos Ethernet No hay información de su dispositivo en dmesg Otros Partición: Particionando Contraseña Realizando 'emerge system' Realizando el bootstrap Por favor, Introduzca un dispositivo Preparando el chroot Primaria Reiniciar Contraseña Contraseña de Root Ejecutando scripts -post-instalación Guardar Seleccione el perfil de instalación a cargar Seleccione la ruta para guardar el perfil de instalación Servicio Configurar password de root Configurando timezone Configurando y ejecutando el bootloader configurando servicios de inicio Shell Shell       ¿Debería el reloj configurarse como UTC o local? A menos que que halla elegido UTC como Huso Horario, debería escojer Local. ¿Deberían cargarse primero el Mapa
de Teclado 'windowkeys'? Tamaño: Estándar Estática Descripción del paso Paso: Sincronizando el árbol de Portage Las contraseña que introdujo no son iguales Esto configura los mapas que se
deben cargar para teclados
extendidos. En la mayoría de
los casos se puede dejar esto
como está. Esta operación borrará tu unidad y aplicará un esquema de particionamiento recomendado. ¿Seguro quiere hacer esto? Huso Horario Tipo Tipo: Disponible Desmontando  Desempacando el tarball del stage Actializando archivos de configuración Opciones de Usuario Pantalla de Usuario UserID Nobre de Usuario Nombre de usuario Bienvenidos al Instalador de Gentoo Linux Inalámbrica Trabajando... No se pueden crear mas de 4 particiones de tipo primario. Si necesita mas particiones, elimine una o mas y cree particiones lógicas No ha configurado su nombre de host y/o su dominio dns Tiene un sistema de archivos montado en %(drive)s. Por favor desmonte el dispositivo antes de realizar cualquier operacion en él. Si no desmonta el dispositivo puede ocurrir pérdida de datos. Ud. ha sido advertido No ha configurado ningún dispositivo. ¿Continuar? Debes especificar un punto de montaje Debes especificar un valor para el campo de dispositivo Debes especificar un punto de montaje en / para continuar _Cancelar estática 