��          �      l      �  ~  �  v   `  A  �  �    �      �
  G  �  a    �   m  `  �  W  X  �  �  )  �   �   �!      C"  r   d"  o   �"  g   G#  5   �#  T   �#  9  :$    t%  �   �&  S  '  0  �(  �  ,    �/  u  �1  �  "5  �   �<  l  �=  N  ?  �  _@  "  
G  p   -H     �H  t   �H  �   I     �I     J  �   5J                                                
                	                                  
<b><u>Extra Packages</u></b>

All of the packages listed on the right are available for the installer to install directly from the LiveCD (including dependencies) without access to the internet.

If you choose a graphical desktop such as gnome, kde, or fluxbox, be sure to also select xorg-x11 from the list. Otherwise, you will not have a fully functioning graphical environment.
 
<b><u>Install Complete!</u></b>

Your install has finished. Click the Exit button, restart your computer, and enjoy!
 
<b><u>Install Failed</u></b>

Your install has failed for one of numerous reasons. You can find the error in the logfile at /var/log/install.log.failed. Once you determine that the error was not caused by you, please file a bug at http://bugs.gentoo.org/ in the Gentoo Release Media product and the Installer component.
 
<b><u>Install Mode</u></b>

Welcome to the GTK+ front-end for the Gentoo Linux Installer. It is highly recommended that you have gone through the manual install process a time or two, or at least read through the install guide.

There are 3 install modes you can choose from: Networkless, Standard, and Advanced. The "Networkless" mode is used for installs where you have no access to the internet, or you just want a fast install using what's available on the LiveCD.  The "Standard" mode is used for networked installs. It allows you some flexibility, but it makes safe assumptions on some advanced options. The "Advanced" mode is for people who want to play around with more settings. If you break your install in this mode, don't come crying to us.
 
<b><u>Local Mounts</u></b>

Here, you can add partitions and special devices to mount during (and after) the install. All mountpoints are relative to /mnt/gentoo during the install.

To start, click the button labeled 'Add'. Enter a device name or select if from the list. If you entered a device name yourself, you'll need to fill in the type in the next field. Then enter the mountpoint and mount options (defaults to "defaults"). Click 'Update' to save the new mount.

To edit a mount that you've already added to the list list, select it by clicking on it. Edit any of the fields below and click the 'Update' button. You can remove it from the list by clicking the 'Delete' button.
 
<b><u>Networking</u></b>

All detected interfaces should show up in the list, but you also have the option to type in your own interface. Once you select an interface, select DHCP or Static Configuration.  Then once you have set your network settings make sure to click Save to add the interface to the list.

Wireless support currently is unavailable, but coming soon!  We even have the boxes for it all ready to go.

Don't forget to set a hostname and domain name in the "Hostname / Proxy Information / Other" tab!
 
<b><u>Other Settings</u></b>

Display Manager:
If you installed gnome, choose gdm. If you installed kde, choose kdm. If you installed anything else specified in XSession, choose xdm.

Console Font:
You probably don't want to mess with this.

Extended Keymaps:
You probably don't want to mess with this.

Windowkeys:
If installing on x86 you are safe with Yes, otherwise you'll probably want to say No.

Keymap:
This defaults to "us" if not set (recommended).  If you don't want an English keymap, choose it from the list.

XSession:
Choose this only if you didn't choose gdm or kdm from the Display Manager list.

Clock:
If you chose a local timezone, you'll want to choose "local" for the clock setting. Otherwise if you chose UTC in the Timezone screen, choose UTC here.

Default Editor:
Pick one.  Nano is the default and recommended.
 
<b><u>Partitioning</u></b>

The choices you make on this screen are very important. There are three ways to do your partitioning.

The first method is the Recommended Layout. If you really have no idea how you should lay out your partitions, this is probably the best option. You will need at least 4GB of disk space to use this option. If you have any existing partitions, they will be deleted. Three partitions will be created: /boot (100MB), swap (calculated based on physical memory, up to 512MB), and / (remaining concurrent space).

The second method is the old fashioned one: doing it yourself. The partition edit is fairly straightforward (although, not <i>that</i> straightforward as this help would not need to exist).

The third method is doing it completely "by hand". You can open up a terminal and use <i>fdisk</i>, <i>cfdisk</i>, or some other partitioning program. You will need to create filesystems on all partitions that you create.

The currently active disk is represented by the bar near the top of the screen. If you have more than one disk present in your system, you can change the active disk by choosing another disk from the drop down box labeled 'Devices'. Along the bottom of the screen, there is a color key for the disk representation above.

To view an existing partition's properties, you must select it by clicking it in the bar above. You will get a brief overview of the partition below the bar. To delete it, click the 'Delete' button. You will be asked for confirmation before the partition is removed.

To create a new partition, select some unallocated space in the bar at the top. For a partition for use by Linux, you will want to use ext2, ext3, reiserfs, jfs, or xfs. Ext3 is the recommended type.

Keep in mind that all changes are committed to disk immediately. If you just click the Next button, your partition table will remain untouched.
 
<b><u>Root Password</u></b>

Enter the password for the root user in your new install. Enter it again to confirm it (to prevent typos).
 
<b><u>Startup Services</u></b>

On this screen, you can select services that you would like to startup at boot. Common choices are sshd (remote access) and xdm (graphical login... choose this for kdm, gdm, and entrance, as well). Only services that are provided by a package you already have installed and are not already in a runlevel are displayed.
 
<b><u>Timezone</u></b>

Pick your timezone, or pick UTC. If you dual-boot with Windows you'll want to choose your local timezone. If your BIOS clock is set to local time you'll also want to choose your local timezone.

If you choose a local timezone, you'll want to choose "local" for the clock setting later on in the Other Settings screen.
 
<b><u>Users</u></b>

Working as root on a Unix/Linux system is dangerous and should be avoided as much as possible. Therefore it is strongly recommended to add a user for day-to-day use.

Enter the username and password in respective boxes.  Make sure to type your password carefully, it is not verified. All other fields are optional, but setting groups is highly recommended.

The groups the user is member of define what activities the user can perform. The following table lists a number of important groups you might wish to use: 
<u>Group</u> 		<u>Description</u>
audio 		be able to access the audio devices
cdrom 		be able to directly access optical devices
floppy 		be able to directly access floppy devices
games 		be able to play games
portage 	be able to use emerge --pretend as a normal user
usb 		be able to access USB devices
plugdev 	Be able to mount and use pluggable devices such as cameras and USB sticks
video 		be able to access video capturing hardware and doing hardware acceleration
wheel 		be able to use su

Enter them in a comma-separated list in the groups box.

Optinally you may also specify the user's shell.  The default is /bin/bash.  If you want to disable the user from logging in you can set it to /bin/false. You can also specify the user's home directory (default is /home/username), userid (default is the next available ID), and a comment describing the user.

Make sure to click Accept Changes to save the changes to your user.  They will then show up in the list.
 
This is where you emerge extra packages that your system may need. Packages that
are fetch-restricted or require you to accept licenses (e.g. many
big games) will cause your install to fail. Add additional packages with
caution. These trouble packages can be installed manually after you reboot.
 Choose what window manager you want to start default with X if run with xdm, startx, or xinit. (common options are Gnome or Xsession) Choose your default console font Choose your display manager for Xorg-x11 (note you must make sure that package also gets installed for it to work) Enter a space separated list of extra packages to install on the system ( in addition to those checked above ): Should CLOCK be set to UTC or local? Unless you set your timezone to UTC you will want to choose local. Should we first load the 'windowkeys' console keymap? This sets the maps to load for extended keyboards. Most users will leave this as is. Project-Id-Version: gli-gtk 2007.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-12-18 12:14-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
 
<b><u>Paquetes Adicionales</u></b>

Los paquetes en la lista de la derecha están disponibles también para ser instalados desde el LiveCD, incluyendo con sus dependencias, sin tener acceso a internet.

Si elije instalar un escritorio gráfico como Gnome, KDE o Fluxbox, debe elegir también xorg-x11 de la lista. De otra manera, no tendrá un entorno gráfico totalmente funcional
 
<b><u>¡Instalación Completa!</u></b>

La instalación ha terminado. Salga del programa, reinicie su computadora, y disfrute de Gentoo!
 
<b><u>Instalación Fallida</u></b>

Su instalación puede haber fallado por numerosas razones. Puede encontrar el error en la bitácora /var/log/install.log.failed. Una vez determinado que el problema no fue causado por Ud., por favor abra una incidencia en http://bugs.gentoo.org/ en los componentes Gentoo Release Media and Installer. 
 
<b><u>Modo de Instalación</u></b>

Bienvenidos al front-end GTK+ para el Instalador de Gentoo Linux. Es altamente recomendable que haya pasado por el proceso manual de instalación una o dos veces o que al menos haya leído la guía de instalación.

Existen 3 maneras que puede seleccionar para instalar: Sin Red, Estándar y Avanzada. La manera "Sin Red" es para instalaciones donde no se tiene acceso a internet o si desea una instalación rápida utilizando lo que se tiene en el LiveCD. La manera "Estándar" es utilizada para instalaciones con red. Permite alguna flexibilidad pero asume, de manera segura, sobre ciertas opciones avanzadas. La manera "Avanzada" es para personas que deseen jugar con muchas mas opciones. Si usted estropea su instalación en este modo de instalación, no venga a llorarnos. 
 
<b><u>Puntos de Montaje Locales</u></b>

Aquí puede agregar particiones y dispositivos especiales para que sean montados durante (y luego) de la instalación. Todos los puntos de montaje serán relativos a /mnt/gentoo durante el proceso de instalación.
 
Para comenzar, presione el botón 'Agregar'. Introduzca un nombre de dispositivo desde la lista. Si introdujo un nombre de dispositivo que no se encuentra en la lista, debe rellenar el tipo de dispositivo en el siguiente campo. Luego, introduzca el punto y las opciones de montaje (si quiere opciones por defecto, introduzca "defaults"). Presione el botón 'Actualizar' para guardar el nuevo punto de montaje. 

Para editar un punto de montaje que ya ha agregado a la lista, selecciónelo haciendo clic sobre él. Edite los campos necesarios y luego presione el botón 'Actualizar'. Puede eliminarlo de la lista presionando el botón 'Eliminar'.
 
<b><u>Red</u></b>

Todas las interfaces detectadas se deberían ver en la lista, pero también tiene la opción de señalar su propia interfaz. Una vez seleccionada, puede elegir DHCP o una Configuración Estática. Después de haber configurado la red asegúrese de presionar 'Guardar' para añadir la interfaz a la lista

El soporte Wireless no está disponible ahora, pero lo estará pronto! Aún tenemos las cajas listas para comenzar.

No olvide señalar un hostname y nombre de dominio en la pestaña "Información de Hostname/Proxy "
 
<b><u>Otras Opciones</u></b>

Manejador de Entornos:
Si instaló Gnome, elija GDM. Si instaló KDE, elija KDM. Si instaló cualquier otro entorno especificado en XSession, elija XDM

Fuente de Cónsola:
Probablemente no desee modificar esto.

Mapas de Teclado Extendidos:
Probablemente no desee modificar esto.

Windowkeys:
Si está instalando en un x86 puede elejir 'Si' con confianza, de otra manera quizá deba elejir 'No'.

Mapa de Teclado:
Por defecto se usa "us" como Mapa de Teclado. Si no desea usar un Mapa Inglés, elija el de su preferencia de la lista.

XSession:
Elija esta opción si usted no escogió GDM o KDM de la lista de manejador de entornos.

Reloj:
Si eligió un Huso Horario propio, debería elegir "local" como opción. Si eligió UTC como Huso Horario, debería seleccionar UTC aquí.

Editor por Defecto:
Elija el de su perferencia. Nano es el recomendado.
 
<b><u>Particionando</u></b>

Las elecciones que haga en esta pantalla son muy importantes. Existen tres maneras de particionar.

El primer método es la Configuración Recomendada. Si realmente no tiene idea cómo debe disponer las particiones, ésta es el mejor método. Necesita al menos 4GB de espacio en disco para utilizar esta opción. Si el disco ya se encuentra particionado, las particiones serán eliminadas. Las siguientes particiones serán. creadas: /boot (100MB), swap (calculada en base a la memoria física, hasta 512MB),  y / (con el espacio restante).

El segundo método es el tradicional: hágalo Ud. mismo. La edición de particiones es sencilla (sin embargo, no <i>tan</i> sencilla ya que esta ayuda existe).
 
El tercer método es hacerlo completamente a "mano". Puede abrir un terminal y utilizar <i>fdisk</i>, <i>cfdisk</i>, o cualquier otro editor de particiones. Deberá crear sistemas de archivo en todas las particiones que cree.

El disco activo está representado por la barra cerca del tope de la pantalla. Si tiene más de un disco en su sistema, puede cambiar el disco activo del seleccionador  marcado como 'Dispositivos'. A lo largo de la parte inferior de la pantalla se encuentra la leyenda para la representación de la barra.

Para ver las propiedades de una partición, debe seleccionarla haciendo clic en la barra de representación del disco. Obtendrá con esto una breve descripción de la partición, por debajo de la barra. Para eliminarla, haga clic en el botón 'Eliminar'. Debe confirmar la operación antes de que la partición sea eliminada.

Para crear una nueva partición, seleccione el espacio disponible en la barra de representación. Para crear una partición para Linux, querrá utilizar ext2, ext3, reiserfs, jfs o xfs. Ext3 es el tipo recomendado.

Tenga en cuenta que todos los cambios son aplicados al disco de inmediato. Si presiona el botón de 'Siguiente' sin realizar modificaciones, su tabla de particiones quedará quedará sin cambios.
 
<b><u>Contraseña de Root</u></b>

Introduzca la contraseña para el usuario root de su nueva instalación. Introdúzcala de nuevo  para confirmarla y evitar errores.
 
<b><u>Servicios de Arranque</u></b>

En esta pantalla puede seleccionar los servicios que desee para que arranquen al iniciar el sistema. Algunas opciones comúnes son sshd(acceso remoto) y xdm (login gráfico... puede seleccione para esto kdm, gdm o entrance). Sólo aquellos servicios que han sido instalados y no se encuentran en un runlevel serán mostrados.
 
<b><u>Huso Horario</u></b>

Elija su Huso horario o UTC. Si tiene un arranque dual con Windows debería elejir su huso horario local, al igual que si el reloj de su BIOS está configurado a la hora local.

Si elije un Huso Horario local, debería elejor "local" como opción parael reloj más tarde en la pantalla de Otras Opciones.
 
<b><u>Usuarios</u></b>

Trabajar como root en un sistema Linux/Unix es muy peligroso y debe ser evitado siempre. Por lo tanto, es muy recomendable añadir un usuario para el uso cotidiano del sistema.

Introduzca el nombre de usuario y la contraseá en los espacios respectivos. Asegúrese de introducir su contraseña con cuidado, ésta no se verifica. Las demás son opcionales, pero la configuración de los grupos es altamente recomendada.

Los grupos en los cuales el usuario es miembro definen las tareas que éste puede ejecutar. La siguiente tabla muestra un número de grupos importantes que quizá desee usar para su usuario: 
<u>Grupo</u> 		<u>Descripción</u>
audio 		acceso a los dispositivos de audio
cdrom 		acceso directo a los dispositivos ópticos
floppy 		acceso directo a las unidades de disquette
games 		capacidad para ejecutar juegos
portage 	capacidad para usar emerge --pretend como usuario normal
usb 		capcidad de montar dispositivos USB
plugdev 	capacidad de montar dispositivos enchufables como cámaras y memorias USB
video 		acceso a la captura de video y la aceleración de video por hardware
wheel 		capacidad de usar su

Introduzca los grupos en una lista separada con comas en el campo grupos.

Opcionalmente puede señalar el shell del usuario. El shell por defecto es /bin/bash.  Si desea desactivar al usuario para ingresar al sistema, puede señalarlo como /bin/false.También puede señalar el directorio home del usuario (/home/nombredeusuario)el userid (por defecto se toma el siguiente ID disponible), y una descripción corta del usuario.

Asegúrese de pulsar 'Aceptar Cambios' para guardar las opciones del usuario. Éstas serán mostradas luego en la lista.
 
Ahora se instalarán los paquetes adicionales que su sistema necesite.Aquellos
restringidos o con licencias que se deban aceptar, ocasionarán que la
instalación falle. Tenga cuidado al elegir los paquetes adicionales.
Esos paquetes problemáticos se pueden instalar después del reboot.
 Elija el manejador de ventanas que desea iniciar al ejecutar xdm, startx o xinit. (por ejemplo Gnome o XSession) defecto Elija el manejador de entornos para
Xorg-x11 (debe estar seguro de que
el paquete está instalado para que
funcione) Introduzca una lista separada por espacios con los paquetes adicionales que desea instalar (que no estén en la lista anterior): ¿Debería el reloj configurarse como UTC o local? A menos que que halla elegido UTC como Huso Horario, debería escojer Local. de Teclado 'windowkeys'? Esto configura los mapas que se
deben cargar para teclados
extendidos. En la mayoría de
los casos se puede dejar esto
como está. 