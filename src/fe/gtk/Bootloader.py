# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
from ProgressDialog import *

class Panel(GLIScreen):

	title = "Bootloader"
	active_selection = None
	radio_bootloaders = {}
	_helptext = """
<b><u>Bootloader</u></b>

The bootloaders available depend on the architecture to be installed. For x86, \
the two bootloaders available are grub and lilo.  In amd64, only grub is \
available. Gentoo recommends grub. For those who still like lilo, it is available. \
Both grub and lilo will detect windows partitions.
"""
	bootloaders = {
	  'none':  [ "none" ],
	  'x86':   [ "grub", "lilo" ],
	  'amd64': [ "grub" ],
	  'ppc':   [ "yaboot" ],
	  'hppa':  [ "palo" ],
	  'mips':  [ "arcload", "arcboot", "colo" ],
	  'sparc': [ "silo" ]
	}
	tmpbootloaders = None

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

#		self.tmpbootloaders = self.bootloaders[self.controller.cc.get_arch()]
#		if self.controller.install_type != "networkless":
#			self.tmpbootloaders += self.bootloaders['none']
		self.tmpbootloaders = self.bootloaders[self.controller.cc.get_arch()] + self.bootloaders['none']
		if self.controller.install_type == "networkless":
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Your bootloader will be %s</b>' % self.tmpbootloaders[0])
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)
		else:
			hbox = gtk.HBox(False)
			label = gtk.Label()
			label.set_markup('<b>Choose your bootloader</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=0)
			vert.pack_start(hbox, expand=False, fill=False, padding=20)

			for bootloader in self.tmpbootloaders:
				hbox = gtk.HBox(False, 0)
				if bootloader == self.tmpbootloaders[0]:
					self.radio_bootloaders[bootloader] = gtk.RadioButton(None, bootloader)
				else:
					self.radio_bootloaders[bootloader] = gtk.RadioButton(self.radio_bootloaders[self.tmpbootloaders[0]], bootloader)
				self.radio_bootloaders[bootloader].set_name(bootloader)
				self.radio_bootloaders[bootloader].connect("toggled", self.bootloader_selected, bootloader)
				hbox.pack_start(self.radio_bootloaders[bootloader], expand=False, fill=False, padding=20)
				vert.pack_start(hbox, expand=False, fill=False, padding=20)

#		self.check_install_in_mbr = gtk.CheckButton("Install in MBR")
#		self.check_install_in_mbr.connect("toggled", self.mbr_selected)
#		self.check_install_in_mbr.set_size_request(125, -1)
#		hbox = gtk.HBox(False, 0)
#		hbox.pack_start(self.check_install_in_mbr, expand=False, fill=False, padding=5)
#		tmplabel = gtk.Label("This controls whether the bootloader is installed in the MBR or not")
#		tmplabel.set_line_wrap(True)
#		hbox.pack_start(tmplabel, expand=False, fill=False, padding=20)
#		vert.pack_start(hbox, expand=False, fill=False, padding=15)

#		hbox = gtk.HBox(False, 0)
#		tmplabel = gtk.Label("Boot Drive:")
#		tmplabel.set_alignment(0.0, 0.5)
#		tmplabel.set_size_request(160, -1)
#		hbox.pack_start(tmplabel, expand=False, fill=False, padding=0)
#		self.boot_device_combo = gtk.combo_box_new_text()
#		hbox.pack_start(self.boot_device_combo, expand=False, fill=False, padding=15)
#		vert.pack_start(hbox, expand=False, fill=False, padding=15)

		hbox = gtk.HBox(False, 0)
		tmplabel = gtk.Label("Extra kernel parameters:")
		tmplabel.set_alignment(0.0, 0.5)
		tmplabel.set_size_request(160, -1)
		hbox.pack_start(tmplabel, expand=False, fill=False, padding=0)
		self.kernel_params_entry = gtk.Entry()
		self.kernel_params_entry.set_width_chars(40)
		hbox.pack_start(self.kernel_params_entry, expand=False, fill=False, padding=15)
		vert.pack_start(hbox, expand=False, fill=False, padding=30)

		self.add_content(vert)
		self.boot_devices = None

	def bootloader_selected(self, widget, data=None):
		self.active_selection = data
#		if data == "none":
#			self.check_install_in_mbr.set_sensitive(False)
#		else:
#			self.check_install_in_mbr.set_sensitive(True)

#	def mbr_selected(self, widget, data=None):
#		if self.check_install_in_mbr.get_active():
#			self.boot_device_combo.set_sensitive(True)
#		else:
#			self.boot_device_combo.set_sensitive(False)

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = True
		self.active_selection = self.controller.install_profile.get_boot_loader_pkg() or self.tmpbootloaders[0]
		if self.controller.install_type != "networkless":
			self.radio_bootloaders[self.active_selection].set_active(True)
		kernel_params = self.controller.install_profile.get_bootloader_kernel_args()
		if not "doscsi" in kernel_params.split():
			for mount in self.controller.install_profile.get_mounts():
				if not mount['devnode'].startswith("/dev/sd"): continue
				if mount['mountpoint'] in ("/", "/boot"):
					if kernel_params:
						kernel_params = "doscsi"
					else:
						kernel_params += " doscsi"
					break
		self.kernel_params_entry.set_text(kernel_params)
#		self.check_install_in_mbr.set_active(self.controller.install_profile.get_boot_loader_mbr())
#		self.boot_devices = self.controller.install_profile.get_partition_tables().keys()
#		self.boot_devices.sort()
#		self.boot_device_combo.get_model().clear()
#		boot_device = self.controller.install_profile.get_boot_device()
#		for i, device in enumerate(self.boot_devices):
#			self.boot_device_combo.get_model().append([device])
#			if boot_device == device:
#				self.boot_device_combo.set_active(i)
#		if self.boot_device_combo.get_active() == -1:
#			self.boot_device_combo.set_active(0)

	def next(self):
		self.controller.install_profile.set_boot_loader_pkg(None, self.active_selection, None)
		self.controller.install_profile.set_boot_loader_mbr(None, True, None)
#		if self.check_install_in_mbr.get_active():
#			self.controller.install_profile.set_boot_device(None, self.boot_devices[self.boot_device_combo.get_active()], None)
		self.controller.install_profile.set_bootloader_kernel_args(None, self.kernel_params_entry.get_text(), None)
		progress = ProgressDialog(self.controller, ('install_bootloader', 'setup_and_run_bootloader' ), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("Users")
		else:
			GLIScreen.progress_callback(self, result, data)
