# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk,gobject
import os
import re
from GLIScreen import *
from Widgets import Widgets
import GLIUtility

class Panel(GLIScreen):
	"""
	The Users section of the installer.
	
	@author:    John N. Laliberte <allanonjl@gentoo.org>
	@license:   GPL
	"""
	
	# Attributes:
	title = _("User Settings")
	columns = []
	users = []
	current_users=[]
	_helptext = _("""
<b><u>Users</u></b>

Working as root on a Unix/Linux system is dangerous and should be avoided as \
much as possible. Therefore it is strongly recommended to add a user for \
day-to-day use.

Enter the username and password in respective boxes.  Make sure to type your \
password carefully, it is not verified. All other fields are optional, but \
setting groups is highly recommended.

The groups the user is member of define what activities the user can perform. \
The following table lists a number of important groups you might wish to use: \

<u>Group</u> 		<u>Description</u>
audio 		be able to access the audio devices
cdrom 		be able to directly access optical devices
floppy 		be able to directly access floppy devices
games 		be able to play games
portage 	be able to use emerge --pretend as a normal user
usb 		be able to access USB devices
plugdev 	Be able to mount and use pluggable devices such as cameras and USB sticks
video 		be able to access video capturing hardware and doing hardware acceleration
wheel 		be able to use su

Enter them in a comma-separated list in the groups box.

Optinally you may also specify the user's shell.  The default is /bin/bash.  If \
you want to disable the user from logging in you can set it to /bin/false. You \
can also specify the user's home directory (default is /home/username), userid \
(default is the next available ID), and a comment describing the user.

Make sure to click Accept Changes to save the changes to your user.  They will \
then show up in the list.
""")

	def __init__(self,controller):
		GLIScreen.__init__(self, controller)

		content_str = _("User screen!")
		
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)
		
		# setup the top box that data will be shown to the user in.
		self.treedata = gtk.ListStore(gobject.TYPE_STRING,gobject.TYPE_STRING, gobject.TYPE_STRING, 
										gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING,
										gobject.TYPE_STRING)
			
		self.treeview = gtk.TreeView(self.treedata)
		self.treeview.connect("cursor-changed", self.selection_changed)
		self.columns.append(gtk.TreeViewColumn(_("Username    "), gtk.CellRendererText(), text=1))
		self.columns.append(gtk.TreeViewColumn(_("Groups"), gtk.CellRendererText(), text=2))
		self.columns.append(gtk.TreeViewColumn(_("Shell      "), gtk.CellRendererText(), text=3))
		self.columns.append(gtk.TreeViewColumn(_("HomeDir "), gtk.CellRendererText(), text=4))
		self.columns.append(gtk.TreeViewColumn(_("UserID"), gtk.CellRendererText(), text=5))
		self.columns.append(gtk.TreeViewColumn(_("Comment"), gtk.CellRendererText(), text=6))
		col_num = 0
		for column in self.columns:
				column.set_resizable(True)
				column.set_sort_column_id(col_num)
				self.treeview.append_column(column)
				col_num += 1
		
		self.treewindow = gtk.ScrolledWindow()
		self.treewindow.set_size_request(-1, 125)
		self.treewindow.set_shadow_type(gtk.SHADOW_IN)
		self.treewindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.treewindow.add(self.treeview)
		#vert.pack_start(self.treewindow, expand=False, fill=False, padding=0)
		
		frame = gtk.Frame("")
		frame.set_border_width(10)
		frame.set_size_request(100, 75)
		frame.show()
		
		frame_vert = gtk.VBox(False,0)
		vbox = gtk.VBox(False, 0)
		#frame.add(frame_vert)
		
		hbox = gtk.HBox(False)
		add = gtk.Button(_("Add user"), stock=None)
		add.connect("clicked", self.addu, "add")
		add.set_size_request(150, -1)
		add.show()
		hbox.pack_start(add, expand=False, fill=False, padding=5)
		
		delete = gtk.Button(_("Delete user"), stock=None)
		delete.connect("clicked", self.delete, "delete")
		delete.set_size_request(150, -1)
		delete.show()
		hbox.pack_start(delete, expand=False, fill=False, padding=5)
		
		vbox.pack_start(self.treewindow, expand=True, fill=True, padding=5)
		vbox.pack_start(hbox, expand=False, fill=False, padding=5)
		
		vert.pack_start(vbox, expand=False, fill=False, padding=0)
		
		# setup the first page
		frame_vert = gtk.VBox(False,0)
		
		# three blank hboxes
		hbox = gtk.HBox(False, 0)
		label = gtk.Label("")
		label.set_size_request(150, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		hbox = gtk.HBox(False, 0)
		label = gtk.Label("")
		label.set_size_request(150, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		hbox = gtk.HBox(False, 0)
		label = gtk.Label("")
		label.set_size_request(150, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		# setup the second page
		frame_vert = gtk.VBox(False,0)
#		table = gtk.Table(rows=8, columns=2)
		self.user = {}
		
		hbox = gtk.HBox(False, 0)
		label = gtk.Label(_("Username"))
		label.set_size_request(100, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		self.username = gtk.Entry()
		self.user['username'] = self.username
		self.username.set_size_request(150, -1)
		self.username.set_name("username")
		hbox.pack_start(self.username, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		hbox = gtk.HBox(False, 0)
		label = gtk.Label(_("Password"))
		label.set_size_request(100, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		self.password = gtk.Entry()
		self.user['password'] = self.password
		self.password.set_size_request(150, -1)
		self.password.set_name("password")
		self.password.set_visibility(False)
		hbox.pack_start(self.password, expand=False, fill=False, padding=5)
		# reset button if they want to reset a user password that was
		# loaded from a user profile
		self.reset_pass = gtk.Button(_("Reset loaded password"))
		self.reset_pass.connect("clicked", self.reset_userpass_from_profile, "reset loaded pass")
		self.reset_pass.set_size_request(150, 5)
		self.reset_pass.set_sensitive(False)
		hbox.pack_start(self.reset_pass, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		hbox = gtk.HBox(False, 0)
		label = gtk.Label(_("Groups"))
		label.set_size_request(100, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		self.groups = gtk.Entry()
		self.user['groups'] = self.groups
		self.groups.set_size_request(150, -1)
		self.groups.set_name("groups")
		hbox.pack_start(self.groups, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		hbox = gtk.HBox(False, 0)
		label = gtk.Label(_("Shell"))
		label.set_size_request(100, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		self.shell = gtk.Entry()
		self.user['shell'] = self.shell
		self.shell.set_size_request(150, -1)
		self.shell.set_name("shell")
		hbox.pack_start(self.shell, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		hbox = gtk.HBox(False, 0)
		label = gtk.Label(_("HomeDir"))
		label.set_size_request(100, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		self.homedir = gtk.Entry()
		self.user['homedir'] = self.homedir
		self.homedir.set_size_request(150, -1)
		self.homedir.set_name("homedir")
		hbox.pack_start(self.homedir, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		hbox = gtk.HBox(False, 0)
		label = gtk.Label(_("UserID"))
		label.set_size_request(100, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		self.userid = gtk.Entry()
		self.user['userid'] = self.userid
		self.userid.set_size_request(150, -1)
		self.userid.set_name("userid")
		hbox.pack_start(self.userid, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		hbox = gtk.HBox(False, 0)
		label = gtk.Label(_("Comment"))
		label.set_size_request(100, -1)
		hbox.pack_start(label, expand=False, fill=False, padding=5)
		self.comment = gtk.Entry()
		self.user['comment'] = self.comment
		self.comment.set_size_request(150, -1)
		self.comment.set_name("comment")
		hbox.pack_start(self.comment, expand=False, fill=False, padding=5)
		button = gtk.Button(_("Accept Changes"))
		button.connect("clicked", self.add_edit_user, "add/edit user")
		button.set_size_request(150, -1)
		hbox.pack_start(button, expand=False, fill=False, padding=5)
		frame_vert.add(hbox)
		
		label = gtk.Label(_("Add/Edit a user"))
		
		vert.pack_start(frame_vert, expand=False, fill=False, padding=5)
		self.add_content(vert)

		
	def selection_changed(self, treeview, data=None):
		treeselection = treeview.get_selection()
		treemodel, treeiter = treeselection.get_selected()
		self.edit(treemodel,treeiter)
		#row = treemodel.get(treeiter, 0)
		#print row
	
	def reset(self, widget, data=None):
		self.blank_the_boxes()
	
	def addu(self,widget, data=None):
		self.blank_the_boxes()
		self.password.set_sensitive(True)
		
	def edit(self, treemodel=None,treeiter=None):
		self.blank_the_boxes()
		# retrieve the selected stuff from the treeview
		#treeselection = self.treeview.get_selection()
		#treemodel, treeiter = treeselection.get_selected()
		
		# if theres something selected, need to show the details
		if treeiter != None:
			data={}
			data["password"],data["username"],data["groups"],data["shell"],\
				data["homedir"],data["userid"],data["comment"] = \
				treemodel.get(treeiter, 0,1,2,3,4,5,6)
			
			# fill the current entries
			for entry_box in self.user:
				box=self.user[entry_box]
				box.set_text(data[box.get_name()])
		
			# if the user was loaded from a profile, disable password box, and enable button
			if self.is_user_from_stored_profile(data["username"]):
				self.reset_pass.set_sensitive(True)
				self.password.set_sensitive(False)
			else:
				# if not from a profile, enable password box, disable reset button
				self.reset_pass.set_sensitive(False)
				self.password.set_sensitive(True)

	def delete(self, widget, data=None):
		self.blank_the_boxes()
		self.delete_user()
	
	def add_edit_user(self, widget, data=None):
		# retrieve the entered data
		data={}
		for entry_box in self.user:
			box=self.user[entry_box]
			data[box.get_name()] = box.get_text()
			
		success = self.add_user(data)
		
		# if it was successful, blank the current entries and 
		# ensure password box is sensitive
		if success == True:
			self.blank_the_boxes()
			self.password.set_sensitive(True)
			
	def add_user(self,data):
		return_val = False
		
		# test to make sure uid is an INT!!!
		if data["userid"] != "":
			test = False
			try:
				uid_test = int(data["userid"])
				test = True
			except:
				# its not an integer, raise the exception.
				msgbox=Widgets().error_Box("Error","UID must be an integer, and you entered a string!")
				msgbox.run()
				msgbox.destroy()
		else:
			test = True
			
		if self.is_data_empty(data) and test==True:
			# now add it to the box
			# ( <user name>, <password hash>, (<tuple of groups>), <shell>, 
			#    <home directory>, <user id>, <user comment> )
			
			# if they were previously added, modify them
			if self.is_in_treeview(data['username']):
				list = self.find_iter(data)
				self.treedata.set(list[data['username']],0,data["password"],
									1,data['username'],
									2,data["groups"],
									3,data["shell"],
									4,data["homedir"],
									5,data["userid"],
									6,data["comment"]
									)
			else:
				self.treedata.append([data["password"],data["username"], data["groups"],
										data["shell"], data["homedir"], 
										data["userid"], data["comment"]])
			#self.current_users.append(data['username'])
			return_val=True
		
		return return_val
			
	def delete_user(self):
		# get which one is selected
		treeselection = self.treeview.get_selection()
		treemodel, treeiter = treeselection.get_selected()
		# if something is selected, remove it!
		if treeiter != None:
			row = treemodel.get(treeiter, 1)
			
			# remove it from the current_users
			#self.current_users.remove(row[0])
			
			# remove it from the profile
			self.controller.install_profile.remove_user(row[0])
			
			# remove it from the treeview
			iter = self.treedata.remove(treeiter)
	
	def blank_the_boxes(self):
		# blank the current entries
		for entry_box in self.user:
			box=self.user[entry_box]
			box.set_text("")
	
	def reset_userpass_from_profile(self, widget, data=None):
		# need to remove the user from the loaded profile
		# so that it will hash the password
		self.controller.install_profile.remove_user(self.username.get_text())
		self.users_from_profile.remove(self.username.get_text())
		# make the button sensitive again
		self.reset_pass.set_sensitive(False)
		self.password.set_sensitive(True)
		# clear the password box
		self.password.set_text("")
	
	def is_data_empty(self,data):
		value = True
		for item in data:
			# if any of the items are blank, return False!
			if item == "":
				value = False
			
		return value
	
	def is_user_from_stored_profile(self,username):
		value = False
		users = self.controller.install_profile.get_users()
		for user in users:
			if user[0] == username:
				value = True
		return value
	
	def is_in_treeview(self,username):
		value = False
		# if the username appears in the treestore, display error
		username_list = self.get_treeview_usernames()
		if username in username_list:
			value = True	
			
		return value
	
	def find_iter(self,data):
		data_stor = {}
		treeiter = self.treedata.get_iter_first()
		
		while treeiter != None:
			username = self.treedata.get_value(treeiter, 1)
			if username == data["username"]:
				data_stor[username]=treeiter
			treeiter = self.treedata.iter_next(treeiter)

		return data_stor
	
	def get_treeview_usernames(self):
		data = []
		treeiter = self.treedata.get_iter_first()
		
		while treeiter != None:
			data.append(self.treedata.get_value(treeiter, 1))
			treeiter = self.treedata.iter_next(treeiter)

		return data

	def get_treeview_data(self):
		data = []
		treeiter = self.treedata.get_iter_first()
		
		while treeiter !=None:
			user = self.treedata.get_value(treeiter,1)
			passwd = self.treedata.get_value(treeiter,0)
			
			# if the user was loaded from a profile, do NOT
			# hash the password, but carry it through
			if user not in self.users_from_profile:
				pass_hash = GLIUtility.hash_password(passwd)
			else:
				pass_hash = passwd
				
			groups = self.treedata.get_value(treeiter,2)
			shell = self.treedata.get_value(treeiter,3)
			homedir = self.treedata.get_value(treeiter,4)
			userid = self.treedata.get_value(treeiter,5)
			comment = self.treedata.get_value(treeiter,6)
			try:
				group_tuple = tuple(groups.split(","))
			except:
				# must be only 1 group
				group_tuple = (groups)
			
			data.append([user,pass_hash,group_tuple,shell,homedir,userid,comment])
			treeiter = self.treedata.iter_next(treeiter)
			
		return data
	
	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = True
		self.controller.SHOW_BUTTON_FORWARD = True
		
		self.users_from_profile = [] # This structure can be taken out
		self.users_iprofile = self.controller.install_profile.get_users()
		
		# load the saved users
		for user in self.users_iprofile:
			groups = ",".join(user[2])
			
			# if the uid field is blank, it will be set to None.
			if user[5] == None:
				# it is a tuple, so we have to thaw it and re-harden.
				user_temp = list(user)
				user_temp[5]=""
				user = tuple(user_temp)
				
			self.add_user({'password':user[1], 'username':user[0], 'groups':groups, 
					      'shell':user[3], 'homedir':user[4], 
					      'userid':user[5], 'comment':user[6]})
			# add them to the list so we know it was loaded from the profile
			# to pass the hash through ( this can be taken out and replaced )
			self.users_from_profile.append(user[0])
		
	def next(self):
		# now store all the entered users
		#( <user name>, <password hash>, (<tuple of groups>), <shell>, 
		#  <home directory>, <user id>, <user comment> )
		# retrieve everything
		data = self.get_treeview_data()

		stored_users = []
		# get the array of stored usernames
		for item in list(self.controller.install_profile.get_users()):
			stored_users.append(item[0])
				
		for user in data:
			if user[0] not in self.current_users and user[0] not in stored_users:
				# user is not in stored profile, and hasn't been previously added
				self.controller.install_profile.add_user(None, (user[0], user[1], user[2], user[3], user[4], user[5], user[6]), None)
				#self.current_users.append(user[0])
			elif user[0] in stored_users:
				# user is in stored profile, need to remove, then readd the user
				self.controller.install_profile.remove_user(user[0])
				self.controller.install_profile.add_user(None, (user[0], user[1], user[2], user[3], user[4], user[5], user[6]), None)
				#self.current_users.append(user[0])
				
		progress = ProgressDialog(self.controller, ("set_users", ), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("ExtraPackages")
		else:
			GLIScreen.progress_callback(self, result, data)

