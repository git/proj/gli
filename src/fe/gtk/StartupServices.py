# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
import gobject
import os
from GLIScreen import *

class Panel(GLIScreen):

	title = _("Startup Services")
	_helptext = _("""
<b><u>Startup Services</u></b>

On this screen, you can select services that you would like to startup at boot. \
Common choices are sshd (remote access) and xdm (graphical login... choose this \
for kdm, gdm, and entrance, as well). Only services that are provided by a \
package you already have installed and are not already in a runlevel are \
displayed.
""")

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)
		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

#		self.cur_runlevel = "default"
#		self.runlevels = {}
		self.existing_services = []
		for runlevel in os.listdir(self.controller.install_profile.get_root_mount_point() + "/etc/runlevels/"):
#			self.runlevels[runlevel] = os.listdir(self.controller.install_profile.get_root_mount_point() + "/etc/runlevels/" + runlevel)
			for service in os.listdir(self.controller.install_profile.get_root_mount_point() + "/etc/runlevels/" + runlevel):
				self.existing_services.append(service)
		self.new_services = []
		for service in os.listdir(self.controller.install_profile.get_root_mount_point() + "/etc/init.d/"):
			if service.endswith(".sh"): continue
			if not service in self.existing_services:
				self.new_services.append(service)
		self.new_services.sort()

		self.choice_list = {
		  "alsasound": _("Loads ALSA modules and restores mixer levels"),
		  "apache": _("Common web server (version 1.x)"),
		  "apache2": _("Common web server (version 2.x)"),
		  "distccd": _("Allows your system to help other systems compile"),
		  "hdparm": _("Makes it easy to set drive parameters automatically at boot"),
		  "portmap": _("Necessary for mounting NFS shares"),
		  "proftpd": _("Common FTP server"),
		  "sshd": _("OpenSSH server (allows remote logins)"),
		  "xfs": _("X Font Server (available if xorg-x11 compiled with USE=font-server)"),
		  "xdm": _("Gives you a graphical login which then starts X")
		}

		self.treedata = gtk.ListStore(gobject.TYPE_BOOLEAN, gobject.TYPE_STRING, gobject.TYPE_STRING)
		self.treeview = gtk.TreeView(self.treedata)
		self.toggle_renderer = gtk.CellRendererToggle()
		self.toggle_renderer.set_property("activatable", True)
		self.toggle_renderer.connect("toggled", self.flag_toggled)
		self.columns = []
		self.columns.append(gtk.TreeViewColumn("", self.toggle_renderer, active=0))
		self.columns.append(gtk.TreeViewColumn(_("Service"), gtk.CellRendererText(), text=1))
		self.columns.append(gtk.TreeViewColumn(_("Description"), gtk.CellRendererText(), text=2))
		for column in self.columns:
#			column.set_resizable(True)
			self.treeview.append_column(column)
		self.treewindow = gtk.ScrolledWindow()
		self.treewindow.set_size_request(-1, 400)
		self.treewindow.set_shadow_type(gtk.SHADOW_IN)
		self.treewindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.treewindow.add(self.treeview)
		vert.pack_start(self.treewindow, expand=False, fill=False, padding=10)

		self.services = self.controller.install_profile.get_services()
		if self.services:
			if isinstance(self.services, str):
				self.services = self.services.split(',')
			else:
				self.services = list(self.services)
		else:
			self.services = []
		self.treedata.clear()
		for service in self.new_services:
			self.treedata.append([(service in self.services), service, self.choice_list.get(service, "")])

		self.add_content(vert)

	def flag_toggled(self, cell, path):
		model = self.treeview.get_model()
		model[path][0] = not model[path][0]
		service = model[path][1]
		if model[path][0]:
			if not service in self.services:
				self.services.append(service)
		else:
			if service in self.services:
				self.services.pop(self.services.index(service))

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = True

	def next(self):
		self.controller.install_profile.set_services(None, ",".join(self.services), None)
		progress = ProgressDialog(self.controller, ("set_services", ), self.progress_callback)
		progress.run()

	def progress_callback(self, result, data=None):
		if result == PROGRESS_DONE:
			self.controller.load_screen("OtherSettings")
		else:
			GLIScreen.progress_callback(self, result, data)

