# Copyright 1999-2005 Gentoo Foundation
# This source code is distributed under the terms of version 2 of the GNU
# General Public License as published by the Free Software Foundation, a copy
# of which can be found in the main directory of this project.

import gtk
from GLIScreen import *
import GLIUtility
import URIBrowser
from ProgressDialog import *

class Panel(GLIScreen):

	title = "Stage Selection"
	_helptext = """
<b><u>Stage Selection</u></b>

Installing from stage3 is the recommended (and only supported) method for Gentoo \
installations.  It contains a complete base system and is already optimized for \
whatever architecture/processor specified in the filename.  While Gentoo still \
offers stage1 and stage2 tarballs, the official installation method uses only \
the stage3 tarball.

You have the option of either downloading a stage from the internet or letting \
the installer create a stage3 equivalent on the fly from the contents of the \
LiveCD, and does not involve any downloading, but it can be a bit slow.

To us the URI Browser, select the type (such as http) and then press the "..." next \
to Host to choose a mirror address. Once your host has been selected the path \
below should change automatically and you should be able to browse through the \
folders to find your stage tarball. The path to the tarballs is usually \
/releases/&lt;architecture ex. x86&gt;/current/stages/&lt;subarch&gt;/filename.tar.bz2
"""

	def __init__(self, controller):
		GLIScreen.__init__(self, controller)

		vert = gtk.VBox(False, 0)
		vert.set_border_width(10)

		if self.controller.install_type == "networkless":
			hbox = gtk.HBox(False, 0)
			label = gtk.Label()
			label.set_markup('<b>Your base system will be created from files on the LiveCD.</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=5)
			vert.pack_start(hbox, expand=False, fill=False, padding=10)
		else:
			hbox = gtk.HBox(False, 0)
			label = gtk.Label()
			label.set_markup('<b>Choose a method for creating the base system</b>')
			hbox.pack_start(label, expand=False, fill=False, padding=5)
			vert.pack_start(hbox, expand=False, fill=False, padding=10)

			self.radio_fetchstage = gtk.RadioButton(None, "Fetch stage from internet")
			self.radio_fetchstage.set_name("fetch")
			self.radio_fetchstage.connect("toggled", self.stage_selected, "fetch")
			hbox = gtk.HBox(False, 0)
			hbox.pack_start(self.radio_fetchstage, expand=False, fill=False, padding=5)
			vert.pack_start(hbox, expand=False, fill=False, padding=10)

			hbox = gtk.HBox(False, 0)
			hbox.pack_start(gtk.Label(" "), expand=False, fill=False, padding=15)
			hbox.pack_start(gtk.Label("URI:"), expand=False, fill=False, padding=5)
			self.entry_stage_tarball_uri = gtk.Entry()
			self.entry_stage_tarball_uri.set_width_chars(50)
			hbox.pack_start(self.entry_stage_tarball_uri, expand=False, fill=False, padding=0)
			self.browse_uri = gtk.Button("Browse")
			self.browse_uri.connect("clicked", self.browse_uri_clicked)
			hbox.pack_start(self.browse_uri, expand=False, fill=False, padding=5)
			vert.pack_start(hbox, expand=False, fill=False, padding=0)

			self.radio_dynamicstage = gtk.RadioButton(self.radio_fetchstage, "Build stage from files on LiveCD")
			self.radio_dynamicstage.set_name("dynamic")
			self.radio_dynamicstage.connect("toggled", self.stage_selected, "dynamic")
			hbox = gtk.HBox(False, 0)
			hbox.pack_start(self.radio_dynamicstage, expand=False, fill=False, padding=5)
			vert.pack_start(hbox, expand=False, fill=False, padding=40)

		self.add_content(vert)

	def browse_uri_clicked(self, widget):
		uribrowser = URIBrowser.URIBrowser(self, self.entry_stage_tarball_uri)
		uribrowser.run(self.entry_stage_tarball_uri.get_text())

	def stage_selected(self, widget, data=None):
		if data == "fetch":
			self.entry_stage_tarball_uri.set_sensitive(True)
			self.browse_uri.set_sensitive(True)
		else:
			self.entry_stage_tarball_uri.set_sensitive(False)
			self.browse_uri.set_sensitive(False)

	def activate(self):
		self.controller.SHOW_BUTTON_BACK    = False
		self.controller.SHOW_BUTTON_FORWARD = True
		if self.controller.install_type != "networkless":
			self.entry_stage_tarball_uri.set_text(self.controller.install_profile.get_stage_tarball_uri())

	def next(self):
		if self.controller.install_type == "networkless":
			self.controller.install_profile.set_install_stage(None, 3, None)
			self.controller.install_profile.set_dynamic_stage3(None, True, None)
			self.controller.install_profile.set_grp_install(None, True, None)
		else:
			if self.radio_fetchstage.get_active():
				uri = self.entry_stage_tarball_uri.get_text().strip()
				if not uri:
					msgdlg = gtk.MessageDialog(parent=self.controller.window, type=gtk.MESSAGE_WARNING, buttons=gtk.BUTTONS_OK, message_format="You did not enter a stage tarball URI. You cannot continue until you set this value.")
					resp = msgdlg.run()
					msgdlg.destroy()
					return
				if not GLIUtility.validate_uri(uri):
					msgdlg = gtk.MessageDialog(parent=self.controller.window, type=gtk.MESSAGE_WARNING, buttons=gtk.BUTTONS_OK, message_format="The stage tarball URI you entered does not exist or is invalid. You cannot continue until this value is valid")
					resp = msgdlg.run()
					msgdlg.destroy()
					return
			self.controller.install_profile.set_install_stage(None, 3, None)
			self.controller.install_profile.set_dynamic_stage3(None, self.radio_dynamicstage.get_active(), None)
			self.controller.install_profile.set_stage_tarball_uri(None, self.entry_stage_tarball_uri.get_text(), None)

		self.controller.load_screen("PortageTree")
