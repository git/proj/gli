#!/usr/bin/env python
# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
#
# Zac Medico <zmedico@gentoo.org>
#

import os
import sys
from itertools import izip

def vdb_to_fake_binrepo():

	metadata_keys = ["CATEGORY", "CHOST", "DEPEND",
		"EAPI", "IUSE", "KEYWORDS", "LICENSE", "PF",
		"PROVIDE", "RDEPEND", "PDEPEND", "SLOT", "USE"]

	import portage
	try:
		from portage import xpak
		from portage.util import ensure_dirs
	except ImportError:
		import xpak
		from portage_util import ensure_dirs

	root = portage.settings["ROOT"]
	vartree = portage.db[root]["vartree"]
	bintree = portage.db[root]["bintree"]

	for cpv in vartree.dbapi.cpv_all():
		bintree.prevent_collision(cpv)
		metadata = dict(izip(metadata_keys,
			vartree.dbapi.aux_get(cpv, metadata_keys)))
		xpdata = xpak.xpak_mem(metadata)
		binpkg_tmpfile = os.path.join(bintree.pkgdir,
			cpv + ".tbz2." + str(os.getpid()))
		ensure_dirs(os.path.dirname(binpkg_tmpfile))
		xpak.tbz2(binpkg_tmpfile).recompose_mem(xpdata)
		binpkg_path = bintree.getname(cpv)
		ensure_dirs(os.path.dirname(binpkg_path))
		os.rename(binpkg_tmpfile, binpkg_path)
		bintree.inject(cpv)

def main():
	description = "This program uses the currently installed packages to" + \
		" populate $PKGDIR with a fake binary package repository that is " + \
		"suitable for dependency calculations."
	usage = "usage: %s" % os.path.basename(sys.argv[0])
	from optparse import OptionParser
	parser = OptionParser(description=description, usage=usage)
	options, args = parser.parse_args(args=sys.argv[1:])
	vdb_to_fake_binrepo()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print "Interrupted."
		sys.exit(1)
